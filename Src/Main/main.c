/*
 * Bel LaPointe
 * Sep 12, 2016
 * CSC 726 - Parallel Algorithms
 *
 * main
 *    Gathers arguments and writes
 *    results of program to a file.
 *    Calls on ThreadMaster for the
 *    bulk of the work in the program.
 * 
 */
#include <stdio.h>
#include <stdlib.h>
#include "../Threads/threads.h"
#include "../Threads/steal.h"

/* For lack of a specified output format,
 * Take the b*c 4-byte ints in *a and print the
 * matrix to my_out
 */
void dump(int*,int,int, char*, int);

int main(int argc, char** argv) {
#ifndef _OPENMP
   printf(  "This system does not support openMP.\n"
            "The program will run in serial.\n");
#endif
   /* Gather optional p and max from argv
    * where p is the number of threads 
    * to create and max is the maximum
    * iterations for determining levels.
    * mode determines if it should implement
    * the default #pragma omp for (==0)
    * or work stealing (!=0)
    */
   int max = 1000, p = 4;
   int height = 4000, width = height;
   int mode = 0;
   if(argc==1) {
      printf("USAGE: ./run processors levels divergeCap divisions\n");
      return 0;
   }
   if(argc>1)
      p = atoi(argv[1]);
   if(argc>2)
      max = atoi(argv[2]);
   if(argc>3)
      mode = atoi(argv[3]);
   if(mode!=0)
      mode = 1;
   printf("For %i threads, creating a %ix%i capping at %i\n", p, height, width, max); 
   int *final = malloc(height*width*sizeof(int));
   if(final==NULL) {
      fprintf(stderr, "ERR: malloc for the results array has failed.\n");
      return 1;
   }
   memset(final, 0, height*width*sizeof(int));
   // Call Threads module to do the work
   if(!mode)
      threads(final, max, height, width, p);
   else
      steal(final, max, height, width, p);
   (argc > 4) ? dump(final, height, width, argv[4], max) : dump(final, height, width, "my_out.dat", max);
   free(final);
   return 0;
}

void dump(int *table, int h, int w, char* filename, int max) {
   FILE *fp = fopen(filename, "w");
   char output;
   fwrite(&h, sizeof(h), 1, fp);
   fwrite(&w, sizeof(w), 1, fp);
   fwrite(table, sizeof(table[0]), h*w, fp);
   fclose(fp);
   return;
}

