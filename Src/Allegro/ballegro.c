#include "ballegro.h"

int ball_init(int w, int h) {
   // Init calls
   al_init();
   al_install_keyboard();
   al_install_mouse();
   al_init_font_addon();
   al_init_ttf_addon();
   al_init_primitives_addon();
   // Initialize globals
   dis_w = w;
   dis_h = h;
   color = BLACK; 
   thick = 1;
   font = al_load_ttf_font("./DejaVuSans.ttf", 12, 0);
   ballbackground = false;
   // Event queue
   mouse = malloc(sizeof(ALLEGRO_EVENT_SOURCE));
   keyboard = malloc(sizeof(ALLEGRO_EVENT_SOURCE));
   //    Do event sources need to be malloc?
   eventQueue = al_create_event_queue();
   al_init_user_event_source(&user);
   al_register_event_source(eventQueue, &user);
   mouse = al_get_mouse_event_source();
   al_register_event_source(eventQueue, mouse);
   keyboard = al_get_keyboard_event_source();
   al_register_event_source(eventQueue, keyboard);
   // Flags
   al_set_new_display_flags(ALLEGRO_WINDOWED);
   al_set_new_display_flags(ALLEGRO_RESIZABLE);
   al_set_new_display_option(ALLEGRO_SWAP_METHOD, 0, ALLEGRO_SUGGEST);
   al_set_new_display_option(ALLEGRO_UPDATE_DISPLAY_REGION, 0, ALLEGRO_SUGGEST);
   // 32bit colors, with alpha
   al_set_new_display_option(ALLEGRO_RED_SIZE, 8, ALLEGRO_SUGGEST);
   al_set_new_display_option(ALLEGRO_GREEN_SIZE, 8, ALLEGRO_SUGGEST);
   al_set_new_display_option(ALLEGRO_BLUE_SIZE, 8, ALLEGRO_SUGGEST);
   al_set_new_display_option(ALLEGRO_ALPHA_SIZE, 8, ALLEGRO_SUGGEST);
   al_set_new_display_option(ALLEGRO_COLOR_SIZE, 32, ALLEGRO_SUGGEST);
   // Spawn the window
   display = al_create_display(dis_w, dis_h);
   al_register_event_source(eventQueue, al_get_display_event_source(display));
   // Clear the window
   al_clear_to_color(al_map_rgb_f(1.0,1.0,1.0));
   al_flip_display();
   // I'm awesome
   al_set_window_title(display, "Belz Win");
   al_set_window_position(display, 50, 50);
   return 0;
}
int ball_clean() {
   aeq_clear();
   al_destroy_display(display);
}

void setColor(ALLEGRO_COLOR beta) {
   color = beta;
}
void setThick(float beta) {
   thick = beta;
}
void getWindowSize(int *w, int *h) {
   *w = al_get_display_width(display);
   *h = al_get_display_height(display);
}
void setWindowSize(int w, int h) {
   al_resize_display(display, w, h);
   resizeCanvas();
}

void line(float x1, float y1, float x2, float y2) {
   al_draw_line(x1, y1, x2, y2, color, thick);
   al_flip_display();
}

void rect(float x1, float y1, float x2, float y2) {
   al_draw_filled_rectangle(x1, y1, x2, y2, color);
   al_flip_display();
}

void circ(float x, float y, float r) {
   al_draw_filled_circle(x, y, r, color);
   al_flip_display();
}

int stringHeight() {
   return al_get_font_line_height(font);
}
int stringWidth(char* phrase) {
   return al_get_text_width(font, phrase);
}
void alwrite(char* str, int x, int y) {
   al_draw_text(font, color, (float)x, (float)y, ALLEGRO_ALIGN_LEFT, str);
   al_flip_display();
}

int aeq_pop() {
   int j;
   ALLEGRO_EVENT a;
   al_wait_for_event(eventQueue, &a);
   j = a.type;
   return j;
}
int aeq_clear() {
   al_flush_event_queue(eventQueue);
   return 1;
}
int decoder(int event) {
   int ret;
   int i, j, k;
   float ap, bp;
   static bool needRedraw = false;
   if( /*event!=EVNT_RESIZE &&*/ needRedraw ) {
      resizeCanvas();
      needRedraw = false;
   }
   switch(event) {
      case EVNT_RESIZE:
         al_acknowledge_resize(display);
         needRedraw = true;
         break;
      default:
         //printf("Event = %i\n", event);
         break;
   }
   return event;
   return ret;
}

bool resizeCanvas() {
   float j, k, ap, bp;
   editCanvas();
   dis_w = al_get_display_width(display);
   dis_h = al_get_display_height(display);
   ap = al_get_bitmap_width(backup);
   bp = al_get_bitmap_height(backup);
   //       > for "center whole image
   //       < for "fill window"
   if(ap/bp < (float)dis_w/dis_h) {
      j = dis_w;
      k = ap;
   } else {
      j = dis_h;
      k = bp;
   }
   ap *= j/k;
   bp *= j/k;
   al_draw_scaled_bitmap(
      backup
      , 0, 0 
      , al_get_bitmap_width(backup), al_get_bitmap_height(backup)
      , dis_w/2.0-ap/2.0, dis_h/2.0-bp/2.0
      , ap, bp
      , 0
   );
   loadCanvas();
   return true;
}

bool saveCanvas() {
   if(backup==NULL) {
      fprintf(stderr, "MSG: Src/Allegro/ballegro.c:saveCanvas():Creating bitmap for backup\n");
      al_set_new_bitmap_format(ALLEGRO_PIXEL_FORMAT_ANY_32_WITH_ALPHA);
      al_set_new_bitmap_flags(ALLEGRO_MEMORY_BITMAP);
      backup = al_create_bitmap(dis_w, dis_h);
   }
   ALLEGRO_BITMAP *ptr = al_get_target_bitmap();
   al_set_target_bitmap(backup);
   al_draw_bitmap(canvas, 0, 0, 0);
   al_set_target_bitmap(ptr);
}

bool newCanvas() {
   ballbackground = true;
   if(canvas==NULL) {
      fprintf(stderr, "MSG: Src/Allegro/ballegro.c:newCanvas():Creating bitmap for canvas\n");
      al_set_new_bitmap_format(ALLEGRO_PIXEL_FORMAT_ANY_32_WITH_ALPHA);
      al_set_new_bitmap_flags(ALLEGRO_MEMORY_BITMAP);
      canvas = al_create_bitmap(dis_w, dis_h);
   }
   al_lock_bitmap(canvas
      , ALLEGRO_PIXEL_FORMAT_ANY_32_WITH_ALPHA
      , ALLEGRO_LOCK_READWRITE);//WRITEONLY);
   al_set_target_bitmap(canvas);
   // al_clear_to_color(al_map_rgb_f(1.0,1.0,1.0));
   ALLEGRO_COLOR old = color;
   setColor(WHITE);
   rect(0,0,dis_w,dis_h);
   setColor(old);
   return true;
}
bool editCanvas() {
   if(canvas==NULL)
      newCanvas();
   al_set_new_bitmap_format(ALLEGRO_PIXEL_FORMAT_ANY_32_WITH_ALPHA);
   al_set_new_bitmap_flags(ALLEGRO_MEMORY_BITMAP);
   ballbackground = true;
   canvas = al_clone_bitmap(al_get_backbuffer(display));
   al_lock_bitmap(canvas
      , ALLEGRO_PIXEL_FORMAT_ANY_32_WITH_ALPHA
      , ALLEGRO_LOCK_READWRITE);//WRITEONLY);
   al_set_target_bitmap(canvas); 
   return true;
}
bool loadCanvas() {
   al_set_new_bitmap_flags(ALLEGRO_MEMORY_BITMAP);
   al_unlock_bitmap(canvas);
   if(backup==NULL)
      saveCanvas();
   ballbackground = false;
   al_set_target_bitmap(al_get_backbuffer(display));
   al_draw_bitmap(canvas, 0, 0, 0);
   al_flip_display();
   return true;
}

void pixl(int x, int y) {
   al_put_pixel(x, y, color);
}
