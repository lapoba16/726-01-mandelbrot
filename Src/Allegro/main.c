#include "ballegro.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

void oldTests();

int main(int argc, char** argv) {
   int w, h, i, j, k;
   int rhash = 51
      , ghash = 63//47
      , bhash = 37;
   FILE *fp;
   // Obligatory file opening checks
   if(argc>1)
      fp = fopen(argv[1], "r"); 
   else
      fp = fopen("my_out.dat", "r");
   if(fp==NULL) {
      printf("ERR: file %s could not be opened.\n"
         , argc>1 ? argv[1] : "my_out");
      return 1;
   }
   // Read "image" dimensions
   // ** outdated, this was for ascii ** //
   /*if(fscanf(fp, "%i", &w)<1) {
      fclose(fp);
      printf("ERR: Couldn't read column count.\n");
      return 1;
   }
   if(fscanf(fp, "%i", &h)<1) {
      fclose(fp);
      printf("ERR: Couldn't read row count.\n");
      return 1;
   }*/
   if(!fread(&h, sizeof(h), 1, fp)) {
      fprintf(stderr, "ERR: couldn't read height of image.\n");
      return -1;
   }
   if(!fread(&w, sizeof(w), 1, fp)) {
      fprintf(stderr, "ERR: couldn't read width of image.\n");
      return -1;
   }
   printf("%i x %i items.\n", w, h);
   // Ready window
   ball_init(w, h);
   // read the wxh items and draw the wxh items
   editCanvas();
   i = -1;
   // outdated from ascii file while( ++i < w*h && fscanf(fp, "%i", &j) != EOF) {
   int *z = malloc(h*w*sizeof(h));
   if(!fread(z, sizeof(int), h*w, fp)) {
      fprintf(stderr, "ERR: Couldn't read a %ix%i image.\n", h, w);
      return -1;
   }
   while(++i < w*h) {
      //fread(&j, sizeof(j), 1, fp);
      j = z[i];
      if(j!=k)
         switch(j) {
            case 0:  setColor(RED);       break;
            case 1:  setColor(ORANGE);    break;
            case 2:  setColor(YELLOW);    break;
            case 3:  setColor(GREEN);     break;
            case 4:  setColor(BLUE);      break;
            case 5:  setColor(PINK);      break;
            case 6:  setColor(PURPLE);    break;
            default: setColor(BLACK);     break;
         }
      if(j!=k)
         if((argc>2 && j==atoi(argv[2])) || argc<2)
            setColor(al_map_rgb_f(
               j*rhash + rhash*rhash
               , j*ghash + ghash*ghash
               , j*bhash + bhash*bhash
            ));
      else
         setColor(BLACK);
      k = j;
      pixl(i/w, i%w);
   }
   saveCanvas();
   setWindowSize(600, 600.0*w/h);
   loadCanvas();
   // Wait till keypress(es), then exit
   int event;
   int tries = 5;
   do {
      event = aeq_pop(); 
      decoder(event);
      if(event==EVNT_KEYUP)
         printf("Tries = %i\n", --tries);
   } while (tries>0);
   fclose(fp);
   ball_clean();  // X error here, inevitable it really is
   return 0;
}


void oldTests() {
   int w = 400, h = 300;
   int i, j;
   // Create a window of the given dimensions
   ball_init(w, h);
   // Draw a rectangle from (10,10) to (20,50)
   rect(10, 10, 20, 50);
   // Draw a line from (35,46) to (75,65)
   line(35, 45, 75, 65);
   // Draw a LIGHTBLUE circle centered in window
   //    and 1/6 of smaller dimension as radius
   setColor(LIGHTBLUE);
   circ(w/2.0, h/2.0, (w<h)?w/6.0:h/6.0);
   // Until keyup, continue capturing events
   int event;
   do {
      event = decoder(aeq_pop());;
      if(event!=20) //  skip mouse move events
         printf("Event #%i\n", event);
   } while( event != 12 );
   // Test creating an image in the background and loading it
   newCanvas();
   setColor(RED);
   rect(w/4, h/4, 3*w/4, 3*h/4);
   setColor(GREEN);
   for(i=w/3; i<2*w/3; i++)
      for(j=h/3; j<2*h/3; j+=5)
         pixl(i, j);
   loadCanvas();
   do aeq_pop(); while (event!=12);
   // Cleanup
   ball_clean();
   // Inevitable allegro crash (X11 error, known and unsolved)
   return;  
}
