#include <allegro5/allegro.h>
#include <allegro5/allegro_color.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_image.h>
#include <stdio.h>

// Colors
#define DARKRED al_map_rgb_f(.5,0,0)
#define RED al_map_rgb_f(1,0,0)
#define LIGHTRED al_map_rgb_f(1,.5,.5)

#define ORANGE al_map_rgb_f(1,.65,0)

#define YELLOW al_map_rgb_f(1,1,0)

#define DARKGREEN al_map_rgb_f(0,.5,0)
#define GREEN al_map_rgb_f(0,1,0)
#define LIGHTGREEN al_map_rgb_f(.5,1,.5)

#define DARKBLUE al_map_rgb_f(0,0,.5)
#define BLUE al_map_rgb_f(0,0,1)
#define LIGHTBLUE al_map_rgb_f(.5,.5,1)

#define PINK al_map_rgb_f(1,0,1)
#define PURPLE al_map_rgb_f(.5,.25,.8)

#define WHITE al_map_rgb_f(1,1,1)
#define LIGHTGRAY al_map_rgb_f(.75,.75,.75)
#define GRAY al_map_rgb_f(.5,.5,.5)
#define DARKGRAY al_map_rgb_f(.25,.25,.25)
#define BLACK al_map_rgb_f(0,0,0)

// Event codes
#define EVNT_KEYDOWN 10
#define EVNT_KEYUP 12
#define EVNT_MOVEMOUSE 20
#define EVNT_MOUSEDOWN 21
#define EVNT_MOUSEUP 22
#define EVNT_FOCUS 23
#define EVNT_MOUSEOFF 24
#define EVNT_RESIZE 41
#define EVNT_DEFOCUS 46

// Globals
ALLEGRO_DISPLAY *display;
ALLEGRO_EVENT_SOURCE *keyboard; 
ALLEGRO_EVENT_SOURCE *mouse;
ALLEGRO_EVENT_SOURCE user;
ALLEGRO_EVENT_QUEUE *eventQueue;
ALLEGRO_FONT *font;
ALLEGRO_COLOR color;
ALLEGRO_BITMAP *canvas;
ALLEGRO_BITMAP *backup;
bool ballbackground;
float thick;
int dis_w, dis_h;

// Obligatory
int ball_init(int, int);
int ball_clean();

// Config
void setColor(ALLEGRO_COLOR);
void setThick(float);
void getWindowSize(int*, int*);
void setWindowSize(int, int);

// Canvas
bool editCanvas();
bool loadCanvas();
bool saveCanvas();
bool newCanvas();
bool resizeCanvas();

// Drawing
void line(float x1, float y1, float x2, float y2);
void rect(float x1, float y1, float x2, float y2);
void circ(float x, float y, float r);
void pixl(int x, int y);

// Writing
int stringHeight();
int stringWidth(char* phrase);
void alwrite(char* str, int x, int y);

// Events
int aeq_pop();
int aeq_clear();
int decoder(int);
// al_get_mouse_state(&ALLEGRO_MOUSE_STATE a);
//    a.buttons&1    a.buttons&2    a.butons&4 ...
//    a.x = mouse X pos             a.y = mouse Y pos
//    a.w = scroll up-down          a.z = scroll left-right
//    a.pressure = [0.0..1.0]
// al_get_mouse_cursor_position(int *x, int *y)
// al_set_mouse_xy(diplay, x, y);
// http://liballeg.org/a5docs/trunk/mouse.html#al_set_system_mouse_cursor

