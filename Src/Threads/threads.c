#include "threads.h"

int threads(int *output, int maxTrial, int height, int width, int processorCnt) {
   // Assert output array is valid
   if(output==NULL) {
      fprintf(stderr, "ERR: Src/Threads/threads.c: threads(): output array is null\n");
      return -2;
   }
   // parallelize
   int i, j, cnt = -1;
   double a, b;
   tic();
   #pragma omp parallel for \
      num_threads(processorCnt) \
      private(i, j, a, b, cnt) \
      schedule(runtime)//,100)  
   for(i=0; i<height; i++) {
      if(!((++cnt)%100)) {
         printf(".");
         if(omp_get_thread_num()==0)
            fflush(stdout);
      }
      a = -2.0 + 4.0/(height-1) * i;
      for(j=0; j<width; j++) {
         b = -2.0 + 4.0/(width-1) * j;
         output[i*width + j] = diverges(a, b, maxTrial);
         if(output[i*width+j]==maxTrial)
            output[i*width+j] = 0;
      }
   }
   toc();
   printf("   %2i   %.2f\n", processorCnt, etime());
   return 0;
}

int diverges(double a, double b, int maxTrial) {
   // struct complex defined in ../Complex/complex.h
   struct complex z, y;
   z.a = z.b = 0.0;
   y.a = a;
   y.b = b;
   int counter = 0;
   // until diverges (when |z|>=2),
   // z_i+1 = z_i*z_i + a+bi
   // while(z.a < 2.0 && z.a > -2.0 && ++counter<maxTrial) {
   // if height doesn't work nicely with floats,
   //    z.a != 2.0 due to the height used to calculate a
   //    (bits bits bits!)
   while(z.a*z.a+z.b*z.b < 4.0 && ++counter<maxTrial) {
      z = complexAdd(complexMul(z, z), y);
   }
   return counter;
}
