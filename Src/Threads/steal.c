#include "steal.h"

int steal(int *output, int maxTrial, int height, int width, int threadcnt) {
   // One lock per thread a queue to hold 
   //    the current and last task per iteration.
   omp_lock_t qlock[threadcnt];
   int q[threadcnt*2];
   int remains=height;
   // Populate q with its initial start/end tasks
   distributeWork(q, threadcnt, height);
   for(int i=0; i<threadcnt; i++)
      omp_init_lock(&qlock[i]);
   tic();
   #pragma omp parallel \
      num_threads(threadcnt) \
      shared(remains) //reduction(-:remains) 
   {
      int id = omp_get_thread_num();
      int start = q[2*id];
      int stop;
      int tmp;
      while(getLoad(q, qlock, &start, &stop, threadcnt, id)) {
         //printf("%2i : %3i..%3i\n", id, start, stop);
         divergeThru(output, start, stop, maxTrial, height, width);
         start = stop;
      }
      //printf("%i escaped\n", id);
   }
   toc();
   for(int i=0; i<threadcnt; i++)
      omp_destroy_lock(&qlock[i]);
   printf("%i   %.2f\n", threadcnt, etime());
   return 0;
}

void distributeWork(int *store, int p, int n) {
   // Given n tasks and p threads,
   //    distribute evenly regardless
   //    of n%p to each thread
   int load;
   int modded = n%p;
   for(int i=0; i<p; i++) {
      if(i<modded) {
         load = n/p + 1;
         store[2*i] = i * load;
      }
      else {
         load = n / p;
         store[2*i] = (load+1) * modded
            + (i-modded) * load;
      }
      store[2*i+1] = store[2*i] + load;
   }
}

void divergeThru(int *output, int start, int stop, int maxTrial, int height, int width) {
   double a, b;
   int j;
   // Solve for rows start..stop-1
   for( ; start < stop; start++) {
      // a+bi's 'a' is -2..2 divided into height segments
      a = -2.0 + 4.0/(height-1.0) * start;
      for(j=0; j<width; j++) {
         // a+bi's 'b' is -2..2 divided into width segments
         b = -2.0 + 4.0/(width-1.0) * j;
         // check how many interations to diverge, cap at max
         output[start*width + j] = diverges(a, b, maxTrial);
         // if iterations==max, put 0 for "didn't make it"
         if(output[start*width+j]==maxTrial)
            output[start*width+j] = 0;
      }
   }
}

bool getLoad(int *q, omp_lock_t *qlock, int *start, int *stop, int p, int id) {
   // Need an r relatively prime to p. Unless it's
   //    1001 threads, this will do.
   int r = (p % 7) ? 7 : 11;
   r = (p % r) ? r : 13;
   // Checking self first, if there's work to do, then
   //    take from the checkID. If no work, return false.
   for(int j=0; j<p; j++) {
      int checkID = (id + j*r) % p;
      int oldEnd, newEnd;
      // If the checked thread is A.) itself, or B.) has more than
      //    30 tasks remaining, take work from check. If B, take
      //    the target's latter half as a new queue for self.
      // try-try-and-lock
      if(q[2*checkID+1] - q[2*checkID] > 30 ) {
         omp_set_lock(&qlock[checkID]);
         if(checkID!=id) omp_set_lock(&qlock[id]);
         if( (checkID==id && q[2*id+1]-q[2*id]>0) 
            || (q[2*checkID+1] - q[2*checkID] > 30)
         ) {
            if(checkID!=id) {
               q[2*id+1] = q[2*checkID+1];
               *start = q[2*checkID+1] = (q[2*checkID]+q[2*checkID+1])/2;
            } else {
               *start = q[2*id];
            }
            // Get 1/3 of remaining queue if it's more than 30 
            //    or finish the set
            *stop = (q[2*id+1] + *start + *start) / 3 + 1;
            *stop = (*stop - *start > 30) ? *stop : q[2*id+1];
            q[2*id] = *stop;
            if(checkID != id) omp_unset_lock(&qlock[id]);
            omp_unset_lock(&qlock[checkID]);
            return true;
         }
         omp_unset_lock(&qlock[checkID]);
         if(checkID!=id) omp_unset_lock(&qlock[id]);
      }
   }
   return false;
}
