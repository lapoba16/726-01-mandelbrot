#ifdef _OPENMP
#include <omp.h>
#endif
#include "../Complex/complex.h"
#include "../Time/etime.h"
#include <string.h>

/*
 * int threads(int* a, int c, int d, int e, int f)
 * Description: threads() takes its arguments and runs
 *    the nested for loops 0..d, and 0..e times
 *    to determine which values cause the equation
 *    to diverge.
 *    It calls diverges() to calculated the diverge value
 *    in no more than c iterations.
 * Pre: *a points to an allocated
 *    array of d rows and e columns. c is the
 *    maximum iterations to check for divergence
 *    and b is variety of outputs in *a. f is
 *    the number of threads to use.
 * Post: *a has been filled with values 0..c-1
 *    in its d rows by e columns. f threads have
 *    been created, done their work, and destroyed.
 */
int threads(int*, int, int, int, int);

/*
 * int diverges(double a, double b, int c)
 * Description: diverges() takes the complex number
 *    formed by a and b (as in a+bi) and a maximum
 *    number of iterations c and determines how many
 *    iterations it takes to diverge when used in
 *       z_0 = 0
 *       z_i+1 = z_i * z_i + y
 *    where y is a+bi. Divergence is defined as
 *    |z_k| >= 2.
 * Pre: a and b are within the scope of the
 *    assignment (a,b = [-2,2]), unchecked,
 *    and c isn't an eternity.
 *    Dependent on struct complex, complexAdd()
 *    and complexMul() defined in Src/Complex/complex.h
 * Post: returns a value less than or equal
 *    to c to reflect the iterations to reach
 *    divergence.
 */
int diverges(double, double, int);




