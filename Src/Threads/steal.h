#ifdef _OPENMP
#include <omp.h>
#endif
#include "threads.h"

typedef enum {false, true} bool;

/*
 * int steal(int *a, int m, int h, int w, int p)
 * Description: steal() takes its arguments and
 *    runs the nested for loops 0..h and 0..w
 *    times to determine which values cause the
 *    equation to diverge (calculated in
 *    diverges() in ./threads.h). It caps diverge's
 *    iterations at 'm'. 'p' is the number of
 *    openMP threads to use.
 * Pre: *a has been allocated with h*w contiguous
 *    slots. m is suggested to be greater than 0.
 *    p is at least 1.
 * Post: *a has been filled with values 0..m-1 in
 *    its h*w elements. 'p' threads have been created
 *    and destroyed.
 */
int steal(int *, int, int, int, int);

/*
 * void distributeWork(int *store, int p, int n)
 * Description: Given an int array of 2*p elements
 *    and n tasks to distribute, populate the
 *    array such that worker i will begin
 *    on task store[2*i] and end on task 
 *    store[2*i+1].
 * Pre: the given array has 2*p elements.
 * Post: the array has been populated and all its
 *    contents have been overwritten.
 */
void distributeWork(int *, int, int);

/*
 * void divergeThru(int *out, int start, int stop, int max, int h, int w)
 * Description: Diverge through will calculate the
 *    iterations to diverge for rows start..stop-1
 *    by calling Src/Threads/threads.?:diverges()
 *    capping at max iterations.
 * Pre: out exists with h*w elements, start>=0
 * Post: out's start..stop-1 rows' elements are
 *    filled.
 */
void divergeThru(int*, int, int, int, int, int);

/*
 * bool getLoad(int *q, lock *qlock, int *start, int *stop, int p, int id)
 * Description: getLoad() will change *start and *stop
 *    are adjust the *q for thread id to calculate
 *    divergence for rows *start..*stop-1. If 
 *    necessary, getLoad() will take from other id's
 *    pool from q with qlocks to stay thread friendly.
 * Pre: *q has 2*p elements with [2*j] and [2*j+1]
 *    start and stop points for thread j at work.
 *    *qlock are all initialized. _OPENMP is defined.
 * Post: q has now been changed to reflect the *start
 *    and *stop returned to thread id; rather, 
 *    *start..*stop-1 are no longer listed as available
 *    in any queue.
 */
bool getLoad(int*, omp_lock_t *, int *, int *, int, int);
