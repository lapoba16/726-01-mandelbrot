#include "../Threads/threads.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
   // Have to check threads() and diverges()
   // diverges() is called in threads, but whatever not the point
   double a, b;
   int max;
   a = -2;
   b = -2;
   max = 15;
   if(argc>1)
      a = atof(argv[1]);
   if(argc>2)
      b = atof(argv[2]);
   if(argc>3)
      max = atoi(argv[3]);
   printf("Diverges = %i on %.2f, %.2f, %i\n", diverges(a, b, max), a, b, max);
   return 0;
}
