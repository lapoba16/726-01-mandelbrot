#include "../Complex/complex.h"
#include <stdio.h>

int main() {
   struct complex x, y;
   x.a = 5;
   x.b = 2;
   y.a = 1;
   y.b = 1;
   printf("x = 5+2i\n"
          "y = 1+1i\n");
   printf("%.1f+%.1fi = x+y\n", complexAdd(x,y).a, complexAdd(x,y).b);
   printf("%.1f+%.1fi = x*y\n", complexMul(x,y).a, complexMul(x,y).b);
   return 0;
}
