#include <fstream>
#include <iostream>
#include <stdio.h>
using namespace std;

void printBits(size_t const size, void const * const ptr, FILE *out)
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;

    for (i=size-1;i>=0;i--)
    {
        for (j=7;j>=0;j--)
        {
            byte = (b[i] >> j) & 1;
            fprintf(out, "%u", byte);
        }
    }
    fputs("", out);
}


int main() {
   ofstream ofs;
   int a=5, b=6;
   ofs.open("my_out");
   ofs.write((const char *)&a, sizeof(int));
   ofs.write((const char *)&b, sizeof(int));
   ofs.close();
   FILE* out;
   out = fopen("my_out2", "w");
   // printBits(sizeof(a), &a, out);
   fwrite(&a, sizeof(a), 1, out); 
   fclose(out);
   return 0;
}
