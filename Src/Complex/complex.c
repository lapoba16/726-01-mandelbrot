#include "complex.h"

struct complex complexAdd(struct complex x, struct complex y) {
   // "compound literal", never used before, no promises
   return (struct complex){   .a = x.a+y.a,  .b = x.b+y.b   };
}

struct complex complexMul(struct complex x, struct complex y) {
   struct complex z;
   /*
    * Have to FOIL and whatnot.
    * (xa + xbi)(ya + ybi)
    * xa*ya + xa*ybi + xbi*ya + xbi*ybi
    * xa*ya + (xa*yb + xb*ya)i - xb*yb
    */
   z.a = x.a * y.a - x.b * y.b;
   z.b = x.a * y.b + x.b * y.a;
   // That was much easier than I expected. Neat.
   return z;
}
