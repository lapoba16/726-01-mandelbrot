#pragma once
#include <stdio.h>

struct complex {
   double a, b;
};

/*
 * complex complexAdd(complex x, complex y)
 * Description: A backwoods implementation of an
 *    addition operator for my backwoods implementation
 *    of a complex struct in C. Returns another complex.
 * Pre: x and y exist.
 * Post: Returns (x.a+y.a, x.b+y.b)
 */
struct complex complexAdd(struct complex, struct complex);

/*
 * complex complexMul(complex x, complex y)
 * Description: A backwoods implementation of a
 *    multiplication operator for my backwoods implementation
 *    of a complex struct in C. Returns another complex.
 * Pre: x and y exist.
 * Post: returns (x.a+x.b*i)(y.a+y.b*i) simplified
 */
struct complex complexMul(struct complex, struct complex);
