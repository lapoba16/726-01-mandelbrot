CC=gcc-5       #  xubuntu
CC=gcc-6       #  osx
CFLAG=-fopenmp -O3
BALLFLAG= `pkg-config --cflags --libs allegro-5 allegro_main-5 allegro_color-5 allegro_primitives-5 allegro_ttf-5 allegro_acodec-5 allegro_audio-5 allegro_dialog-5 allegro_image-5 allegro_memfile-5 allegro_physfs-5 allegro_font-5`

#============================

PROGS = run \
   ballTest

all: ${PROGS}

#============================

FILES_PIECES = main.o complex.o threads.o etime.o steal.o #ballegro.o
run: $(FILES_PIECES) 
	$(CC) -o $@ $(CFLAG) $(FILES_PIECES) && rm $(FILES_PIECES)

FILES_BALLTEST = ballegro.o Src/Allegro/main.c
ballTest: $(FILES_BALLTEST)
	$(CC) -o $@ $(CFLAG) $(FILES_BALLTEST) $(BALLFLAG) && rm ballegro.o

C_COMPLEX = Src/Complex/complex.c
FILES_COMPLEX = Src/Complex/complex.h $(C_COMPLEX)

C_THREADS = Src/Threads/threads.c
FILES_THREADS = Src/Threads/threads.h $(C_THREADS) complex.o

C_STEAL= Src/Threads/steal.c
FILES_STEAL= Src/Threads/steal.h $(C_THREADS) threads.o

C_MAIN = Src/Main/main.c
FILES_MAIN = $(C_MAIN) steal.o 

C_BALL = Src/Allegro/ballegro.c
FILES_BALL = Src/Allegro/ballegro.h $(C_BALL)

complex.o: $(FILES_COMPLEX)
	$(CC) -c $(CFLAG) -o $@ $(C_COMPLEX)

main.o: $(FILES_MAIN)
	$(CC) -c $(CFLAG) -o $@ $(C_MAIN)

threads.o: $(FILES_THREADS)
	$(CC) -c $(CFLAG) -o $@ $(C_THREADS)

steal.o: $(FILES_STEAL)
	$(CC) -c $(CFLAG) -o $@ $(C_STEAL)

etime.o: Src/Time/etime.h Src/Time/etime.c
	$(CC) -c $(CFLAG) -o $@ Src/Time/etime.c

ballegro.o: $(FILES_BALL)
	$(CC) -c $(CFLAG) -o $@ $(C_BALL) $(BALLFLAG)

#===========================

clean:
	rm -f *.o ${PROGS} my_out*
