/*
   Bel LaPointe
   Apr 22, 2016
   CSC 646 - Parallel Programming
   
   Compile
      make
   Run
      Where 'P' is the number of open mp threads to use,
      
      ./ompmotif P motifFile sequenceFile outputFile
      
   ompseq (distribute sequences)
      Given a series of motifs and sequences, this program
      will count how many sequences contain each motif. This
      version uses openmp to parallelize the tasks by distributing
      the sequences amongst the threads. Each thread receives
      a complete copy of the motifs.
      This program writes updates to stderr as it runs.
*/
#include "include/all.h"
#ifdef _OPENMP
#include <omp.h>
#endif
/* countmatches will compare two given char*s
   allocated to fit a 2d array to see if the motifs
   in one exist in the other's sequences, where
   'X' in the motifs is considered a wildcard
   PRE:  each * is already allocated and loaded
         input_ms_cntlen[2]%P == 0 (the number
         of sequences evenly divides by the number
         of threads)
   POST: matchcnt contains the frequency of
         each motif in the selection of the
         sequence array
*/
bool countmatches(int *input_ms_cntlen, char* mots, char* seqs, int* matchcnt, int P);

int main(int argc, char** argv)
{
   #ifndef _OPENMP
   sprintf(stderr, "NOTE: Openmp not available on this machine.\n");
   #endif
   char *motifs, *sequences;
   /* [0] = motifs cnt
      [1] = motifs len
      [2] = sequence cnt
      [3] = sequence len
   */
   int ms_cntlen[4], i, j;
   bzero(ms_cntlen, 4*sizeof(int));
   if(argc<5){
      printf("Usage: ./ompmotif P motifFile seqFile outputFile\n");
      return 1;
   }
   int P = atoi(argv[1]);
   if(!read_in(argv[2], argv[3], &motifs, &sequences, ms_cntlen) ){
      fprintf(stderr, "ERR: could not read in\n");
      return 2;
   }
   fprintf(stderr, "Read in %i motifs length %i and %i sequences length %i\n", ms_cntlen[0], ms_cntlen[1], ms_cntlen[2], ms_cntlen[3]);
   //    ALLOCATE SPACE FOR RESULTS
   // split on motifs, so shared matchcnt is okay
   // need as many matches as there are motifs (ms_cntlen[0])
   int matchcnt[ms_cntlen[0]];
   bzero(matchcnt, sizeof(int)*(ms_cntlen[0]) );
   //    COUNT THE MATCHES
   tic();
   countmatches(ms_cntlen, motifs, sequences, matchcnt, P);
   toc();
   fprintf(stderr, "Completed in %f seconds (%.1f minutes).\n", etime(), (etime()/60));
   if(!print_out(argv[4], motifs, ms_cntlen, matchcnt)){
      fprintf(stderr, "ERR: could not print to %s\n", argv[4]);
      return 5;
   }
   free(motifs);
   free(sequences);
   return 0;
}

bool countmatches(int *ms_cntlen, char* mots, char* seqs, int* matchcnt, int P)
{
   char onemotif[ms_cntlen[1]+1], onesequence[ms_cntlen[3]+1];
   bool matching = false;
   int i, j, k;
   int motif_cnt = ms_cntlen[0]; 
   int seq_cnt = ms_cntlen[2];
   // Split the sequences among all threads
   /*
      Each thread gets a chunk of seq_cnt/P sequences.
      Each thread needs its own iterators for each
      loop, its own "current item" char[]s, and its
      own "match" boolean to determine if its current
      items match each other. Each thread also needs
      its int[motif_cnt] array to track how many of
      its sequences matched any particular motif. Join
      these items later in the shared matchcnt[] array.
      Keep in mind critical section.
   */
   #pragma omp parallel num_threads(P) \
      private(i) private(j) private(k) \
      private(onesequence) private(onemotif) private(matching)
   {
      // each thread has its own results
      int mine[ms_cntlen[0]];
      bzero(mine,sizeof(int)*ms_cntlen[0]);
      // each thread does seq_cnt/P items
      #pragma omp for
      for(i=0; i<seq_cnt; i++){
         // each thread has its own current sequence
         strcpy( onesequence, getstr(seqs, i, ms_cntlen[3]+1));
         for(j=0; j<motif_cnt; j++){
            // each thread has its own current motif
            strcpy( onemotif, getstr(mots, j, ms_cntlen[1]+1));
            matching=true;
            // each thread determines if its sequence/motif match
            for(k=0; k<ms_cntlen[1]; k++)
               if(onemotif[k]!='X' && onemotif[k] != onesequence[k]){
                  matching=false;
                  break;
               }
            // +1 if matched, +0 if not
            mine[j] += matching;
         }
      }
      // join each thread's results into one global array
      // may have been faster to make the whole loop critical?
      for(i=0; i<motif_cnt; i++)
         #pragma omp atomic
         matchcnt[i] += mine[i];
   }
}
