// pthreads
   #include <pthread.h>
   void* foobar(void*);
   int N, P;
   pthread_mutex_t mutex, barr;
   pthread_cond_t cond;
   int main(int argc, char** argv)
   {
      int i;
      P = atoi(argv[1]);
      N = 5;
      pthread_t *thread_handles = malloc(P * sizeof(pthread_t));
      for(i=0; i<P; i++)
         pthread_create(
            &thread_handles[i]
            , NULL
            , foobar
            , (void*)((long)i)
         );
      for(i=0; i<P; i++)
         pthread_join( thread_handles[i], NULL);
      return 0;
   }   
   void* foobar (void* input) 
   {
      long my_rank = (long)input;
      int my_load = N / P + ( (N%P > my_rank) ? 1 : 0);
      int my_min_point = my_rank * (N / P);
      my_min_point += (N%P > my_rank) ? my_rank : N%P;
      int my_max_point = (my_min_point + my_load < N) ? my_min_point+my_load : N;
      pthread_mutex_lock(&mutex);
      critical_section();
      pthread_mutex_unlock(&mutex);
      /************BARRIER**************/
      pthread_mutex_lock(&barr);
      thrd_cntr++;
      if(thrd_cntr==P){
         thrd_cntr=0;
         pthread_cond_broadcast(&cond);
      }
      else{
         while(pthread_cond_wait(&cond, &barr) != 0);
      }
      pthread_mutex_unlock(&barr);
      return NULL;
   }
// MPI
   #include <mpi.h>

   int main(int argc, char** argv)
   {
      int P, my_rank, array[4];
      MPI_Init(&argc, &argv);
      MPI_Comm_size(MPI_COMM_WORLD, &P);
      MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
      array[0]=array[1]=array[2]=array[3]=my_rank;
      MPI_Barrier(MPI_COMM_WORLD);
      MPI_Bcast(array, 4, MPI_INT, 0, MPI_COMM_WORLD);   /* from 0 to all  */
      MPI_Scatter(   /* from, to, from thread #, comm */
         array, 4, MPI_INT,
         array, 4, MPI_INT, 
         0, MPI_COMM_WORLD
      );
      MPI_Gather(   /* from, to, to thread #, comm */
         array, 4, MPI_INT,
         array, 4, MPI_INT, 
         0, MPI_COMM_WORLD
      );
      MPI_Finalize();
      return 0;
   }
// OpenMP
   #ifdef _OPENMP
   #include <omp.h>
   #endif
   #include <stdio.h>
   int main()
   {
      int i, sum;
      /* schedule, chunksize always optional
         (static, chunksize)
            everyone gets N / P tasks total, chunksize at a time
         0 <- 0..0
         1 <- 1..1
         0 <- 2..2
         1 <- 3..3
         (dynamic, chunksize)
            first come, first served queue, chunksize at a time
         0 <- 0..0
         1 <- 1..1
         1 <- 2..2
         0 <- 3..3
         (guided, chunksize)
            first come, first served queue, decreasing chunks minimum chunksize
         0 <- 0..5
         1 <- 6..8
         0 <- 9..9
         1 <- 10..10
         (auto)
            compiler choses
         (runtime)
            ./a.out P OMP_SCHEDULE="type,chunksize"
      */
      /* Critical Sections (best to worst)
         #pragma omp parallel ... reduction(+:i)
            can be +,-,*,&,^,!,| 
            written "i _= item;" where _ becomes operation
         #pragma omp atomic
         #pragma omp critical
      */
      sum=0;
      #pragma omp parallel for num_threads(2) \
      private(i) reduction(+:sum)
      for(i=0; i<10; i++){
         #ifdef _OPENMP
         printf("task #%i on thread %i / %i\n", i, omp_get_thread_num(), omp_get_num_threads());
         #else
         printf("task #%i unparallelized\n", i);
         #endif
         sum += i;
      }
      printf("Calculated sum(0..10) = %i\n", sum);
      return 0;
   }