#include "fileIO.h"

FILE* manage_file(char* file_name, char* type)
{
   FILE *thisfile = fopen(file_name, type);
   if (!thisfile){
      if(type[0]=='r'){
         fprintf(stderr, "ERR: couldn't read file '%s'.\n", file_name);
         return NULL;
      }
      else if (type[0]=='w'){
         fprintf(stderr, "ERR: couldn't write to file '%s'.\n", file_name);
         return NULL;
      }
      return NULL;
   }
   return thisfile;
}

bool read_in(char* motifInName, char* seqInName, char** motifs, char** sequences, int ms_cntlen[4])
{
   int i, j;
   if( !( read_one_file(motifInName, motifs, ms_cntlen, 0, 1) ) )
      return false;
   if( !( read_one_file(seqInName, sequences, ms_cntlen, 2, 3) ) )
      return false;
   return true;
}
bool read_one_file(char* filename, char** inthisarray, int ms_cntlen[], int a, int b)
{
   FILE* input;
   int i, j;
   if( ! (input = manage_file(filename, "r"))){
      printf("bad input file name '%s'\n", filename);
      return false;
   }
   j=fscanf(input, "%i %i", &ms_cntlen[a], &ms_cntlen[b]);
   char test[ms_cntlen[b]+1];
   (*inthisarray) = (char*)malloc(ms_cntlen[a] * (ms_cntlen[b]+1) * sizeof(char*));
   for (i=0; i<ms_cntlen[a]; i++){
      j=fscanf(input, "%s", test);
      for(j=0; j<ms_cntlen[b]; j++)
         set((*inthisarray), i, j, ms_cntlen[b]+1, test[j]);
      set((*inthisarray), i, ms_cntlen[b], ms_cntlen[b]+1, '\0');
   }
   fclose(input);
   return true;
}
bool print_out(char* name, char* motifs, int ms_cntlen[], int* matchcnt)
{
   int i, j;
   FILE* output;
   if( ! (output = manage_file(name, "w"))){
      printf("bad output file name '%s'\n", name);
      return false;
   }
   fprintf(output, "%i\n", ms_cntlen[0]);
   for(i=0; i<ms_cntlen[0]; i++){
      fprintf(output, "%s", getstr(motifs, i, ms_cntlen[1]+1));
      fprintf(output, ",%i\n", matchcnt[i]);
   }
   fclose(output);
   return true;
}
