#include "get.h"

char get(char* array, int a, int b, int rowsize)
{
   // array is (char**)malloc(maxrow*maxcol*sizeof(char))
   return *(char*)(array+a*rowsize*sizeof(char)+b*sizeof(char));
}

void set(char* array, int a, int b, int rowsize, char me)
{
   *(char*)(array+a*rowsize*sizeof(char)+b*sizeof(char)) = me;
}

char* getstr(char *array, int a, int rowsize)
{
   return (char*)(array+a*rowsize*sizeof(char));
}