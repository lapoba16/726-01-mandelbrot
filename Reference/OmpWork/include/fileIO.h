#ifdef __cplusplus
extern "C"{
#endif

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <math.h>
#include <pthread.h>
#include "get.h"
#ifndef PERSONAL_BOOLEAN
#define PERSONAL_BOOLEAN
typedef enum { false, true } bool;
#endif

/* read_in takes the file names of the motifs and
   sequences files, two char** to store the motifs
   and sequences, and an int[4] to store the counts
   and lengths of the inputs.
   PRE:  only the int[4] array has been allocated
   POST: the program must end if this returns false
         the char**s have been allocated enough space
*/
bool read_in(char*, char*, char**, char**, int[]);
/* print_out takes the name of a file, an
   allocated char* containing the motifs, the
   populated int[] with the number and lengths
   of strings int he char*, and an int* with the
   frequency for each motif in the char*
   PRE:  the char*, int[], and int* are fully
         populated, with the char* and int*
         having int[0] rows and the char* with
         int[1] columns (single-dimension 
         allocation of a two-dimensional array)
   POST: the program ends if this returns false
*/
bool print_out(char*, char*, int [], int*);
/* read_one_file takes a file name, an unallocated
   char** to store the results, an int[] to tell
   how much, and indeces int a and int b to use
   the int[].
   PRE:  the char** is unallocated, the int[] is
         fully loaded, and int a, int b are valid
         indeces (and meaningful) from int[]
   POST: the char** has been fully allocated
         the program ends if this returns false
*/
bool read_one_file(char*, char**, int [], int, int);
FILE* manage_file(char* file_name, char* type);

#ifdef __cplusplus
}
#endif
