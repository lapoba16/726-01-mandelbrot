#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

using namespace std;

int attf_stringheight(ALLEGRO_FONT *f, string phrase="") {
   return al_get_font_line_height(f);
}
int attf_stringwidth(ALLEGRO_FONT *f, string phrase) {
   return al_get_text_width(f, phrase.c_str());
}
void attf_write(ALLEGRO_FONT *f, string phrase, 
   int x, int y, int align = ALLEGRO_ALIGN_LEFT) {
   al_draw_text(
      f,    // allegro font
      al_map_rgb_f(0,0,0),    // allegro color
      (float)x,      // x
      (float)y,      // y
      align,         // alignment
      phrase.c_str()
   );
   al_flip_display();
}