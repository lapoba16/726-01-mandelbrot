#include "../allegro_setup.h"
#include "../allegro_queue.h"
#include "../allegro_ttf.h"
#include "../allegro_draw.h"
#include "allegro_decode.h"
#include "../default_colors.h"
#include <unistd.h>     // for usleep(microsecs); usleep(5000);
using namespace std;

/*#ifndef MyWindowClass
#define MyWindowClass
class Window {
public:
   ALLEGRO_DISPLAY* screen;
   ALLEGRO_EVENT_QUEUE* events;
   ALLEGRO_EVENT_SOURCE *user, *mouse;
   int rate, width, height;
   ALLEGRO_FONT *font12;
};
#endif*/
void solidflip();
void test_attf(Window*);
void test_ad(Window*);

int main(int argc, char** argv){
   Window* window = allegro_init(800, 600);
   ALLEGRO_EVENT* happen = NULL;
   test_ad(window);
   test_attf(window);
   while(1) {
      aeq_wait(window->queue, &happen);
      if(happen==NULL)  continue;
      printf("Type = %i\n", happen->type);
      switch(happen->type){
         case LMOUSEDOWN_EVENT:
            //solidflip();
            break;
         case RESIZE_EVENT:
            al_acknowledge_resize(window->screen);
            break;
      }
      allegro_update();
   }
   allegro_destroy(window);
   return 0;
}

void solidflip(){
   static bool state = false;
   state = !state;
   if(state)
      al_clear_to_color(al_map_rgb_f(.1,.1,.1));
   else 
      al_clear_to_color(al_map_rgb_f(.7,.7,.7));
}
void test_attf(Window* window){
   attf_write(window->font12, "Lapointe", 15, 15);
   attf_write(window->font18, "Lapointe", 15, 
      15+attf_stringheight(window->font12));
}
void test_ad(Window* window){
   //ALLEGRO_COLOR lightblue = al_map_rgb_f(.5,.5,1);
   ad_line(15, 15+attf_stringheight(window->font12)+
      attf_stringheight(window->font18),
      15+attf_stringwidth(window->font18, "Lapointe"),
      15+attf_stringheight(window->font12)+
      attf_stringheight(window->font18), 
      0
   );
   ad_line(
      15, 
      25+attf_stringheight(window->font12)+
         attf_stringheight(window->font18),
      15+attf_stringwidth(window->font18, "Lapointe"),
      25+attf_stringheight(window->font12)+
         attf_stringheight(window->font18) 
   );
   ad_line(
      15, 
      35+attf_stringheight(window->font12)+
         attf_stringheight(window->font18),
      15+attf_stringwidth(window->font18, "Lapointe"),
      35+attf_stringheight(window->font12)+
         attf_stringheight(window->font18),
      5, LIGHTBLUE 
   );
   ad_rect(
      15, 
      15,
      15+attf_stringwidth(window->font12, "Lapointe"),
      15+attf_stringheight(window->font12, "Lapointe"),
      RED, 3, false
   );
   ad_rect(
      15,
      15+attf_stringheight(window->font12),
      15+attf_stringwidth(window->font18, "Lapointe"),
      15+attf_stringheight(window->font12)+
         attf_stringheight(window->font18, "Lapointe"),
      GREEN, 5, true
   );
   ad_circle(
      window->width/3, 2*window->height/3,
      (window->width<window->height)?
         window->width/6-15:window->height/6-15,
      BLUE, 5
   );
   ad_ellipse(
      2*window->width/3, 2*window->height/3,
      1.25*((window->width<window->height)?
         window->width/6:window->height/6),
      .75*((window->width<window->height)?
         window->width/6:window->height/6),
      DARKGRAY, 0, true
   );
}
