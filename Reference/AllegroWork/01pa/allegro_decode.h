#include <stdio.h>
#define MOVEMOUSE_EVENT 20
#define RESIZE_EVENT 41
#define FOCUS_EVENT 23
#define KEYDOWN_EVENT 10
#define KEYUP_EVENT 12
#define LMOUSEDOWN_EVENT 21
#define LMOUSEUP_EVENT 22
#define DEFOCUS_EVENT 46
#define RESIZE_EVENT 41

void allegro_decode_print(ALLEGRO_EVENT* happen) {
   int type = happen->type;
   switch(type){
   case MOVEMOUSE_EVENT:
      printf("%2i Move Mouse\n", type);      
      break;
   case RESIZE_EVENT:
      printf("%2i Resize\n", type);
      break;
   case FOCUS_EVENT:
      printf("%2i Focus\n", type);
      break;
   case KEYDOWN_EVENT:
      printf("%2i Key down\n", type);
      break;
   case KEYUP_EVENT:
      printf("%2i Key up\n", type);
      break;
   case LMOUSEUP_EVENT:
      printf("%2i L Mouse Up\n", type);
      break;
   case LMOUSEDOWN_EVENT:
      printf("%2i L Mouse Down\n", type);
      break;
   default:
      printf("%2i Unknown\n", type);
   }
}
void allegro_mousepos_print() {
   ALLEGRO_MOUSE_STATE ptr;
   al_get_mouse_state(&ptr);
   printf("%i, %i\n", ptr.x, ptr.y);
   if(ptr.buttons<=0)
      printf("No button\n");
   else{
      int i=0;
      while(!(ptr.buttons&1)){
         i++;
         ptr.buttons = ptr.buttons>>1;
      }
      if(i==1)
         printf("1st button\n");
      else if (i==2)
         printf("2nd button\n");
      else if (i==3)
         printf("3rd button\n");
      else
         printf("%ith button\n", i);
   }
}
