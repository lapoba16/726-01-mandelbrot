#include "fetch.h"

string getPage(string url) {
   return getPage(url.c_str());
}
string getPage(char* url) {
   string asdf(url);
   return getPage(asdf);
}

struct MemoryStruct {
   char *memory;
   size_t size;
};

static size_t WriteMemoryCallback(void *contents, 
size_t size, size_t nmemb, void *userp) {
   size_t realsize = size * nmemb;
   struct MemoryStruct *mem = (struct MemoryStruct *)userp;
   mem->memory = (char*)realloc(mem->memory, mem->size + realsize + 1);
   if(mem->memory == NULL) {
      /* out of memory! */
      printf("not enough memory (realloc returned NULL)\n");
      return 0;
   }
   memcpy(&(mem->memory[mem->size]), contents, realsize);
   mem->size += realsize;
   mem->memory[mem->size] = 0;
   return realsize;
}

string getPage(const char* url) {
   CURL *curl_handle;
   CURLcode res;
   struct MemoryStruct chunk;
   chunk.memory = (char*)malloc(1);   /* will be grown as needed by the realloc above */
   chunk.size = 0;      /* no data at this point */
   curl_global_init(CURL_GLOBAL_ALL);
   curl_handle = curl_easy_init();
   curl_easy_setopt(curl_handle, CURLOPT_URL, url);
   curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, 
      WriteMemoryCallback);
   curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, 
      (void *)&chunk);
   curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, 
      "libcurl-agent/1.0");
   curl_easy_setopt(curl_handle, CURLOPT_FOLLOWLOCATION, 1); 
   res = curl_easy_perform(curl_handle);
   if(res != CURLE_OK) {
      fprintf(stderr, "curl_easy_perform() failed: %s\n", 
         curl_easy_strerror(res));
      curl_easy_cleanup(curl_handle);
      free(chunk.memory);
      curl_global_cleanup();
      return "";
   }
   /*
    * Now, our chunk.memory points to a memory block that is chunk.size
    * bytes big and contains the remote file.
    *
    * Do something nice with it!
    */
   string sendmeback(chunk.memory);
   //sendmeback.copy(chunk.memory, chunk.size, 0);
   /*
    */
   //fprintf(stderr, "%lu bytes retrieved\n", (long)chunk.size);
   curl_easy_cleanup(curl_handle);
   free(chunk.memory);
   curl_global_cleanup();
   while (sendmeback[sendmeback.length()-1]=='\n' || 
   sendmeback[sendmeback.length()-2]=='\n') 
      sendmeback = sendmeback.substr(0,sendmeback.length()-1);
   return sendmeback;
}

string fixURL(string input) {
   // http://www.liquidation.com/aucimg/11136/m11136306.html
   //     *    *    *    *    *    *    *    *    *    *
   // http://www.liquidation.com/auction/view?id=11080992
   // if it's the second, make it into the first
   if (input[30]!='i') {
      string newurl = "http://www.liquidation.com/aucimg/";
      input = input.substr(43, 9);
      newurl += input.substr(0,5) + "/m" + input + ".html";
      return newurl;
   }
   return input;
}

vector<Item> parsePage(string input, string url, float scale) {
   int rowcnt = 0;
   bool firstrow = false;
   vector<Item> final;
   final.clear();
   int i=0, length = input.length();
   while(i<length) {
      if(input[i]=='<' && input.substr(i, 3)=="<tr"){
         rowcnt++;
         i+=2;
         while(input[i]!='>')
            i++;
         int j=i+75;
         while(input.substr(j+1, 5)!="</tr>")
            j++;
         if(firstrow)
            final.push_back(itemFromHTMLRow(input.substr(i+1,j-i), scale));
         else
            firstrow = true;
         i = j;
      }
      i++;
   }
   string ordernum = fixURL(url).substr(41,8);
   for(unsigned int z=0; z<final.size(); z++){
      final[z].order_id = atoi(ordernum.c_str());
      if(final[z].name==""){
         final.erase(final.begin()+z);
         z--;
      }
   }
   return final;
}

Item itemFromHTMLRow(string html, float scale) {
   string itemname = "";
   short quantity = -1;
   float retail_price = -1.0;
   int i=0, length = html.length();
   while(i<length) {
      if(html[i]=='<' && html.substr(i, 3)=="<td"){
         i+=2;
         while(html[i]!='>')
            i++;
         int j=i++;
         while(j+6<length && html.substr(j+1, 5)!="</td>"){
            j++;
         }
         if(itemname==""){
            for(int y=i; y<j; y++)
               if((html[y]>='a'&&html[y]<='z') || (html[y]>='A'&&html[y]<='Z')){
                  itemname = html.substr(i,j-i+1) + "";
                  break;
               }
         }
         else if (quantity==-1 && html[i]>='0' && html[i]<='9'){
            quantity = (short)atoi(html.substr(i,j-i+1).c_str());
         }
         else if (html.substr(i,j-i+1).find(".")!=string::npos) {
            if(html[i]=='$')
               i++;
            retail_price = atof(html.substr(i,j-i+1).c_str());
            break;
         }
         i = j+4;
      }
      i++;
   }
   Item determined(itemname, -1, quantity, 
      "",-1,retail_price,retail_price*scale);
   return determined;
}