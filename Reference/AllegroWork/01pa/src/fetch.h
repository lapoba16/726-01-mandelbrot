#pragma once
#ifndef BBARL_FETCH_H
#define BBARL_FETCH_H
#include <curl/curl.h>
#include <stdlib.h>
#include <string.h>
#include "item.h"
using namespace std;

string getPage(string);
string getPage(char*);
string getPage(const char*);

string fixURL(string);
vector<Item> parsePage(string, string, float scale = .5);
Item itemFromHTMLRow(string, float);

#endif
