#include "collection.h"
#include <unistd.h>     // usleep(microsecs)

void mysleep(float a);  // mysleep(seconds)

int main (int argc, char** argv) {
   test_ebay();
   return 0;
   unsigned int maxlink = (argc>1) ? atoi(argv[1]) : 1;
   vector<string> testlocation = {
      "http://www.liquidation.com/auction/view?id=11136306"
      , "http://www.liquidation.com/auction/view?id=11125718"
      , "http://www.liquidation.com/aucimg/11121/m11121865.html"
      , "http://www.liquidation.com/aucimg/11121/m11121865.html"
      , "http://www.liquidation.com/aucimg/11127/m11127312.html"
      , "http://www.liquidation.com/aucimg/11121/m11121865.html"
   };
   vector<Collection> collect;
   for(unsigned int i=0; i<testlocation.size() && i<maxlink; i++)
      collect.push_back(collectFromURL(testlocation[i], (i+1)/10.0));
   Collection combined;
   for(unsigned int i=0; i<collect.size(); i++)
      combined = combined.merge(collect[i]);
   combined.dump(cout);
   combined.filter("month",4).dump(cout);
   combined.filter("year","2016").dump(cout);
   return 0;
}

void mysleep(float a) {
   usleep(a*1000000); //
}