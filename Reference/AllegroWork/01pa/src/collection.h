#pragma once
#ifndef BBARL_COLLECTION_H
#define BBARL_COLLECTION_H
#include "fetch.h"
#include "ebaysync.h"
#include <algorithm>    // sort()
#include <string>
using namespace std;

class Collection {
private:
   void update();
public:
/***Attributes***/
   int id, size;
   string name;
   vector<Item> items;
   time_t created, modified;
/***Methods***/
   Collection(Item anitem = Item(), string given = "", int an_id = -1);
   Collection(vector<Item> data, string given = "", int an_id = -1);
   
   Collection sortItems(string givenCategory = "name", bool ascend = true);
   Collection filter(string, string);
   Collection filter(string, int);
   Collection merge(Collection);
   string summary();
   
   void dump(ostream&);
   void forceUpdate();
};

Collection collectFromURL(string, float scale = .5);

bool bbarl_collection_compareForSort_asc_name(Item, Item);
bool bbarl_collection_compareForSort_desc_name(Item, Item);
bool bbarl_collection_compareForSort_asc_oname(Item, Item);
bool bbarl_collection_compareForSort_desc_oname(Item, Item);
bool bbarl_collection_compareForSort_asc_id(Item, Item);
bool bbarl_collection_compareForSort_desc_id(Item, Item);
bool bbarl_collection_compareForSort_asc_oid(Item, Item);
bool bbarl_collection_compareForSort_desc_oid(Item, Item);
bool bbarl_collection_compareForSort_asc_msrp(Item, Item);
bool bbarl_collection_compareForSort_desc_msrp(Item, Item);
bool bbarl_collection_compareForSort_asc_estimate(Item, Item);
bool bbarl_collection_compareForSort_desc_estimate(Item, Item);
bool bbarl_collection_compareForSort_asc_state(Item, Item);
bool bbarl_collection_compareForSort_desc_state(Item, Item);
bool bbarl_collection_compareForSort_asc_created(Item, Item);
bool bbarl_collection_compareForSort_desc_created(Item, Item);
bool bbarl_collection_compareForSort_asc_modified(Item, Item);
bool bbarl_collection_compareForSort_desc_modified(Item, Item);
bool bbarl_collection_compareForSort_asc_quantity(Item, Item);
bool bbarl_collection_compareForSort_desc_quantity(Item, Item);

#endif
