#include "item.h"

Item::Item( string given, int an_id, int cnt,
string oname, int o_id, float retail, float est, 
char curstate, int* bundled_item_id, int bundle_size ) {
   name = given;
   id = an_id;
   quantity = (short)cnt;
   order_name = oname;
   order_id = o_id;
   MSRP = retail;
   estimate = est;
   state = curstate;
   created = modified = time(NULL);
   bundle.clear();
   if(bundle_size>0)
      for(int i=0; i<bundle_size; i++)
         bundle.push_back(bundled_item_id[i]);
}

string Item::summary() {
   char buff[100];
   stringstream output;
   output.str("");
   
   sprintf(buff, "%12s: %s\n", 
      "Item", name.c_str());
   output << buff;
   sprintf(buff, "%12s: %08i\n", 
      "ID", id);
   output << buff;
   sprintf(buff, "%12s: %s\n", 
      "Order Label", order_name.c_str());
   output << buff;
   sprintf(buff, "%12s: %08i\n", 
      "Order Number", order_id);
   output << buff;
   {
      char altbuff[15];
      sprintf(altbuff, "$%.2f", MSRP);
      sprintf(buff, "%12s: %7s\n", 
         "Retail", altbuff);
      output << buff;
      sprintf(altbuff, "$%.2f", estimate);
      sprintf(buff, "%12s: %7s\n", 
         "Estimate", altbuff);
      output << buff;
   }
   sprintf(buff, "%12s: ", 
      "State");
   output << buff;
   if ( state & State::archived )
      output << "Archived\n";
   else if ( state & State::shipped )
      output << "Shipped\n";
   else if ( state & State::sold )
      output << "Sold\n";
   else if ( state & State::listed )
      output << "Listed\n";
   else
      output << "N/A\n";
   
   sprintf(buff, "%12s: %s\n", 
      "Created", timestring(created).c_str());
   output << buff;
   sprintf(buff, "%12s: %s\n", 
      "Modified", timestring(modified).c_str());
   output << buff;
   if(bundle.size()>0){
      output << "Includes... ";
      sprintf(buff, "%08i", bundle[0]);
      output << buff;
      for(unsigned int i=1; i<bundle.size(); i++){
         sprintf(buff, "%08i", bundle[i]);
         output << ", " << buff;
      }
   }
   sprintf(buff, "%12s: %i", 
      "Quantity", (int)quantity);
   output << buff;
   return output.str();
}

void Item::update() {
   modified = time(NULL);
}

void printStates() {
   printf("States\n");
   printf("   %i = %-s\n", State::null, "null");
   printf("   %i = %-s\n", State::listed, "Listed");
   printf("   %i = %-s\n", State::sold, "Sold");
   printf("   %i = %-s\n", State::shipped, "Shipped");
   printf("   %i = %-s\n", State::archived, "Archived");
}

string timestring(time_t curtime) {
   stringstream asdf;
   string dump;
   asdf.str("");
   time_t rawtime = curtime;
   struct tm* timeinfo;
   timeinfo = localtime(&rawtime);
   asdf << asctime(timeinfo);
   dump = asdf.str();
   return dump.substr(0, dump.length()-1);
}