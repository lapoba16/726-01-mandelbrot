#include "collection.h"

Collection::Collection( Item anitem, 
string given, int an_id ) {
   id = an_id;
   name = given;
   items.clear();
   created = modified = time(NULL);
   if(anitem.name!=""){
      size = 1;
      items.push_back(anitem);
   } else {
      size = 0;
   }
}
Collection::Collection( vector<Item> data, 
string given, int an_id) {
   id = an_id;
   name = given;
   items.clear();
   created = modified = time(NULL);
   items = data;
   size = items.size();
}

void Collection::dump(ostream& os) {
   char buff[100];
   
   sprintf(buff, "%12s: %s\n", 
      "Collection", name.c_str());
   os << buff;
   sprintf(buff, "%12s: %08i\n", 
      "ID", id);
   os << buff;
   sprintf(buff, "%12s: %i\n", 
      "Size", (int)(items.size()));
   os << buff;
   sprintf(buff, "%12s: %s\n", 
      "Created", timestring(created).c_str());
   os << buff;
   sprintf(buff, "%12s: %s\n", 
      "Modified", timestring(modified).c_str());
   os << buff;
   
   /*sprintf(buff, "%12s: %i\n", 
      "Items", (int)(items.size()));
   os << buff;
   
   if(items.size() > 0) {
      sprintf(buff, "%12s: \n", 
         "Items");
      os << buff;
      for(unsigned int i=0; i<items.size(); i++){
         sprintf(buff, "%08d %s (%i @ $%.2f)\n", 
            items[i].id, 
            items[i].name.c_str(),
            items[i].quantity,
            items[i].estimate
         );
         os << buff;
      }
   }*/
}

string Collection::summary() {
   char buff[100];
   stringstream output;
   output.str("");
   
   sprintf(buff, "%12s: %s\n", 
      "Collection", name.c_str());
   output << buff;
   sprintf(buff, "%12s: %08i\n", 
      "ID", id);
   output << buff;
   sprintf(buff, "%12s: %i\n", 
      "Size", (int)(items.size()));
   output << buff;
   sprintf(buff, "%12s: %s\n", 
      "Created", timestring(created).c_str());
   output << buff;
   sprintf(buff, "%12s: %s\n", 
      "Modified", timestring(modified).c_str());
   output << buff;
   
   /*sprintf(buff, "%12s: %i\n", 
      "Items", (int)(items.size()));
   output << buff;
   
   if(items.size() > 0) {
      sprintf(buff, "%12s: \n", 
         "Items");
      output << buff;
      for(unsigned int i=0; i<items.size(); i++){
         sprintf(buff, "%08d %s (%i @ $%.2f)\n", 
            items[i].id, 
            items[i].name.c_str(),
            items[i].quantity,
            items[i].estimate
         );
         output << buff;
      }
   }*/
   string sendmeback = output.str();
   sendmeback = sendmeback.substr(0,sendmeback.length()-1);
   return sendmeback;
}

Collection collectFromURL(string url, float scale) {
   string newurl = fixURL(url);
   string page = getPage(newurl);
   Collection collect(parsePage(page, url, scale));
   collect.id = collect.items[0].order_id;
   return collect;
}

Collection Collection::sortItems(string givenCategory, bool ascend) {
   bool (*foo)(Item, Item);
   /* the ampersand is actually optional */
   foo = &(bbarl_collection_compareForSort_asc_name);
   
   if(ascend){
      if(givenCategory=="name")
         foo = &(bbarl_collection_compareForSort_asc_name);
      else if (givenCategory=="oname")
         foo = &(bbarl_collection_compareForSort_asc_oname);
      else if (givenCategory=="id")
         foo = &(bbarl_collection_compareForSort_asc_id);
      else if (givenCategory=="oid")
         foo = &(bbarl_collection_compareForSort_asc_oid);
      else if (givenCategory=="msrp")
         foo = &(bbarl_collection_compareForSort_asc_msrp);
      else if (givenCategory=="estimate")
         foo = &(bbarl_collection_compareForSort_asc_estimate);
      else if (givenCategory=="state")
         foo = &(bbarl_collection_compareForSort_asc_state);
      else if (givenCategory=="created")
         foo = &(bbarl_collection_compareForSort_asc_created);
      else if (givenCategory=="modified")
         foo = &(bbarl_collection_compareForSort_asc_modified);
      else if (givenCategory=="quantity")
         foo = &(bbarl_collection_compareForSort_asc_quantity);
   } else {
      if(givenCategory=="name")
         foo = &(bbarl_collection_compareForSort_desc_name);
      else if (givenCategory=="oname")
         foo = &(bbarl_collection_compareForSort_desc_oname);
      else if (givenCategory=="id")
         foo = &(bbarl_collection_compareForSort_desc_id);
      else if (givenCategory=="oid")
         foo = &(bbarl_collection_compareForSort_desc_oid);
      else if (givenCategory=="msrp")
         foo = &(bbarl_collection_compareForSort_desc_msrp);
      else if (givenCategory=="estimate")
         foo = &(bbarl_collection_compareForSort_desc_estimate);
      else if (givenCategory=="state")
         foo = &(bbarl_collection_compareForSort_desc_state);
      else if (givenCategory=="created")
         foo = &(bbarl_collection_compareForSort_desc_created);
      else if (givenCategory=="modified")
         foo = &(bbarl_collection_compareForSort_desc_modified);
      else if (givenCategory=="quantity")
         foo = &(bbarl_collection_compareForSort_desc_quantity);
   }
   sort(items.begin(), items.end(), foo);
   return *this;
}

void Collection::update() {
   modified = time(NULL);
}
void Collection::forceUpdate() {
   modified = time(NULL);
}

Collection Collection::filter(string cate, string select) {
   vector<Item> newone;
   newone.clear();
   Collection collect;
   transform(select.begin(), select.end(), 
      select.begin(), ::tolower);
   if(cate=="name"){
      for(unsigned int i=0; i<items.size(); i++){
         string temp = items[i].name;
         transform(temp.begin(), temp.end(), 
            temp.begin(), ::tolower);
         if(temp.find(select)!=string::npos)
            newone.push_back(items[i]);
      }
   } else if (cate=="oname") {
      for(unsigned int i=0; i<items.size(); i++){
         string temp = items[i].order_name;
         transform(temp.begin(), temp.end(), 
            temp.begin(), ::tolower);
         if(temp.find(select)!=string::npos)
            newone.push_back(items[i]);
      }
   } else if (cate=="id") {
      for(unsigned int i=0; i<items.size(); i++){
         string temp = to_string(items[i].id);
         transform(temp.begin(), temp.end(), 
            temp.begin(), ::tolower);
         if(temp.find(select)!=string::npos)
            newone.push_back(items[i]);
      }
   } else if (cate=="oid") {
      for(unsigned int i=0; i<items.size(); i++){
         string temp = to_string(items[i].order_id);
         transform(temp.begin(), temp.end(), 
            temp.begin(), ::tolower);
         if(temp.find(select)!=string::npos)
            newone.push_back(items[i]);
      }
   } else if (cate=="state") {
      char searchstate = 0;
      if(select.find("archive")!=string::npos)
         searchstate = State::archived;
      else if (select.find("ship")!=string::npos)
         searchstate = State::shipped;
      else if (select.find("sold")!=string::npos)
         searchstate = State::sold;
      else if (select.find("list")!=string::npos)
         searchstate = State::listed;
      else
         searchstate = State::null;
      if(searchstate!=State::null)
         for(unsigned int i=0; i<items.size(); i++){
            char temp = items[i].state;
            if(temp & searchstate)
               newone.push_back(items[i]);
         }
      else
         for(unsigned int i=0; i<items.size(); i++){
            char temp = items[i].state;
            if(! temp | searchstate)
               newone.push_back(items[i]);
         }
   } else if (cate=="year") {
      int yearSearch = atoi(select.c_str());
      for(unsigned int i=0; i<items.size(); i++) {
         if(yearSearch == 1900 + localtime(&(items[i].created))->tm_year)
            newone.push_back(items[i]);
      }
   } else if (cate=="month") {
      int monthSearch;
      if(select.find("jan")!=string::npos)         monthSearch = 0;
      else if (select.find("feb")!=string::npos)   monthSearch = 1;
      else if (select.find("mar")!=string::npos)   monthSearch = 2;
      else if (select.find("apr")!=string::npos)   monthSearch = 3;
      else if (select.find("may")!=string::npos)   monthSearch = 4;
      else if (select.find("jun")!=string::npos)   monthSearch = 5;
      else if (select.find("jul")!=string::npos)   monthSearch = 6;
      else if (select.find("aug")!=string::npos)   monthSearch = 7;
      else if (select.find("sep")!=string::npos)   monthSearch = 8;
      else if (select.find("oct")!=string::npos)   monthSearch = 9;
      else if (select.find("nov")!=string::npos)   monthSearch = 10;
      else if (select.find("dec")!=string::npos)   monthSearch = 11;
      else monthSearch = -1;
      for(unsigned int i=0; i<items.size(); i++) {
         //monthA = 1900 + localtime(items[i].modified)->tm_mon;
         if(monthSearch == localtime(&(items[i].created))->tm_mon)
            newone.push_back(items[i]);
      }
   }
   collect = Collection(newone, name, id);
   return collect;
}
Collection Collection::filter(string cate, int select) {
   vector<Item> newone;
   newone.clear();
   Collection collect;
   if (cate=="id") {
      return filter("id", to_string(select));
   } else if (cate=="oid") {
      return filter("oid", to_string(select));
   } else if (cate=="state") {
      char searchstate = select;
      if(searchstate!=State::null)
         for(unsigned int i=0; i<items.size(); i++){
            char temp = items[i].state;
            if( temp & searchstate)
               newone.push_back(items[i]);
         }
      else
         for(unsigned int i=0; i<items.size(); i++){
            char temp = items[i].state;
            if(! temp | searchstate)
               newone.push_back(items[i]);
         }
   } else if (cate=="year")  {
      for(unsigned int i=0; i<items.size(); i++) {
         if(select == 1900 + localtime(&(items[i].created))->tm_year)
            newone.push_back(items[i]);
      }
   } else if (cate=="month") {
      for(unsigned int i=0; i<items.size(); i++) {
         //monthA = 1900 + localtime(items[i].modified)->tm_mon;
         if(select == localtime(&(items[i].created))->tm_mon)
            newone.push_back(items[i]);
      }
   }
   collect = Collection(newone, name, id);
   return collect;
}

Collection Collection::merge(Collection append) {
   Collection newcollect = append;
   newcollect.size += size;
   newcollect.name += " / " + name;
   for(unsigned int i=0; i<items.size(); i++)
      newcollect.items.push_back(items[i]);
   newcollect.created = time(NULL);
   newcollect.modified = time(NULL);
   return newcollect;
}

/************************************************/

bool bbarl_collection_compareForSort_asc_name(Item a, Item b) {
   string aname = a.name;
   string bname = b.name;
   transform(aname.begin(), aname.end(), aname.begin(), ::tolower);
   transform(bname.begin(), bname.end(), bname.begin(), ::tolower);
   return aname < bname;
}
bool bbarl_collection_compareForSort_asc_oname(Item a, Item b) {
   string aname = a.order_name;
   string bname = b.order_name;
   transform(aname.begin(), aname.end(), aname.begin(), ::tolower);
   transform(bname.begin(), bname.end(), bname.begin(), ::tolower);
   return aname < bname;
}
bool bbarl_collection_compareForSort_desc_oname(Item a, Item b) {
   string aname = a.order_name;
   string bname = b.order_name;
   transform(aname.begin(), aname.end(), aname.begin(), ::tolower);
   transform(bname.begin(), bname.end(), bname.begin(), ::tolower);
   return aname > bname;
}
bool bbarl_collection_compareForSort_desc_name(Item a, Item b) {
   string aname = a.name;
   string bname = b.name;
   transform(aname.begin(), aname.end(), aname.begin(), ::tolower);
   transform(bname.begin(), bname.end(), bname.begin(), ::tolower);
   return aname > bname;
}
bool bbarl_collection_compareForSort_asc_id(Item a, Item b) {
   return a.id < b.id;
}
bool bbarl_collection_compareForSort_desc_id(Item a, Item b) {
   return a.id > b.id;
}
bool bbarl_collection_compareForSort_asc_oid(Item a, Item b) {
   return a.order_id < b.order_id;
}
bool bbarl_collection_compareForSort_desc_oid(Item a, Item b) {
   return a.order_id > b.order_id;
}
bool bbarl_collection_compareForSort_asc_msrp(Item a, Item b) {
   return a.MSRP < b.MSRP;
}
bool bbarl_collection_compareForSort_desc_msrp(Item a, Item b) {
   return a.MSRP > b.MSRP;
}
bool bbarl_collection_compareForSort_asc_estimate(Item a, Item b) {
   return a.estimate < b.estimate;
}
bool bbarl_collection_compareForSort_desc_estimate(Item a, Item b) {
   return a.estimate > b.estimate;
}
bool bbarl_collection_compareForSort_asc_state(Item a, Item b) {
   return a.state < b.state;
}
bool bbarl_collection_compareForSort_desc_state(Item a, Item b) {
   return a.state > b.state;
}
bool bbarl_collection_compareForSort_asc_created(Item a, Item b) {
   return a.created < b.created;
}
bool bbarl_collection_compareForSort_desc_created(Item a, Item b) {
   return a.created > b.created;
}
bool bbarl_collection_compareForSort_asc_modified(Item a, Item b) {
   return a.modified < b.modified;
}
bool bbarl_collection_compareForSort_desc_modified(Item a, Item b) {
   return a.modified > b.modified;
}
bool bbarl_collection_compareForSort_asc_quantity(Item a, Item b) {
   return a.quantity < b.quantity;
}
bool bbarl_collection_compareForSort_desc_quantity(Item a, Item b) {   
   return a.quantity > b.quantity;
}