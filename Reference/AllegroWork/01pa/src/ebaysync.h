#pragma once
#ifndef BBARL_EBAYSYNC_H
#define BBARL_EBAYSYNC_H
#include "fetch.h"
using namespace std;

class EbayRecord {
public:
   string title="", viewItemURL="";
   float price=0.0;
   time_t endTime=time(NULL);
};

string default_omfget();
bool ackResultScan(string*, bool&, int&);
void test_ebay();
int resultCount(string*);
vector<EbayRecord> scanPage(string);
time_t parseEbayTime(string);

#endif