#pragma once
#ifndef BBARL_ITEM_H
#define BBARL_ITEM_H
#include <iostream>
#include <time.h>
#include <vector>
#include <sstream>
using namespace std;

enum State : char {
   archived = 0b1000,
   shipped = 0b0100,
   sold = 0b0010,
   listed = 0b0001,
   null = 0b0000
};

class Item {
public:
/***Attributes***/
   string name, order_name;
   int id, order_id;
   float MSRP, estimate;
   char state;   // listed,sold,shipped,archived
   time_t created, modified;
   vector<short> bundle;
   short quantity;
/***Methods***/
   Item( 
      string given = "",      int an_id = -1, 
      int cnt = 1,
      string oname = "",      int o_id = -1,
      float retail = 0.0,     float est = 0.0,
      char curstate = State::null,
      int* bundled_item_id = NULL,  int bundle_size=0
   );
   string summary();
   void update();
};

void printStates();
string timestring(time_t);

#endif