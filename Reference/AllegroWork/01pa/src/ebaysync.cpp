#include "ebaysync.h"

void test_ebay() {
   string ebay = default_omfget();
   vector<EbayRecord> asdf = scanPage(ebay);
}

string default_omfget() {
   return getPage("http://svcs.ebay.com/services/"+
      string("search/FindingService/v1?OPERATION-NAME=fi")+
      "ndItemsIneBayStores&SERVICE-VERSION=1.0.0&SE"+
      "CURITY-APPNAME=BreeRobb-bbarl1pa-PRD-54d8cb0"+
      "2c-ae0bb1d7&RESPONSE-DATA-FORMAT=JSON&REST-PAY"+
      "LOAD&storeName=OMF_s_Garage&sortOrder=EndTimeSoonest");
   return getPage("http://svcs.ebay.com/services/"+
      string("search/FindingService/v1?OPERATION-NAME=")+
      "findItemsIneBayStores&SERVICE-VERSION=1.0"+
      ".0&SECURITY-APPNAME=BreeRobb-bbarl1pa-PRD-"+
      "54d8cb02c-ae0bb1d7&RESPONSE-DATA-FORMAT=JSON"+
      "&REST-PAYLOAD&storeName=OMF_s_Garage&sortOrder"+
      "=EndTimeSoonest&paginationInput.entriesPerPage=3");
   return getPage("http://svcs.ebay.com/services/sea"+
      string("rch/FindingService/v1?OPERATION-NAME=f")+
      string("indItemsIneBayStores&SERVICE-VERSION=1.")+
      "0.0&SECURITY-APPNAME=BreeRobb-bbarl1pa-PR"+
      "D-54d8cb02c-ae0bb1d7&RESPONSE-DATA-FORMAT=JS"+
      "ON&REST-PAYLOAD&storeName=OMF_s_Garage&so"+
      "rtOrder=EndTimeSoonest&keywords=harry");
}

bool ackResultScan(string *a, bool &ack, int &resultcnt) {
   int i=30;
   while(++i<200) {
      if(a->substr(i,3)=="ack"){
         int j = i+1;
         while((*a)[++j]!='[');
         int k = j;
         while((*a)[++k]!=']');
         if(a->substr(j+2,k-j).find("ucce")!=string::npos)
            ack = true;
         else{
            resultcnt = 0;
            ack = false;
            return false;
         }
      }
      if((*a)[i]=='@' && a->substr(i,string("@count").length())=="@count"){
         int j = i+2;
         while((*a)[++j]!='"');
         while((*a)[++j]!='"');
         int k = j;
         while((*a)[++k]!='"');
         resultcnt = atoi(a->substr(j+1,k-j-1).c_str());
         return true;
      }
   }
   return false;
}

vector<EbayRecord> scanPage(string ebay) {
   vector<EbayRecord> base;
   EbayRecord temp;
   string trash;
   bool ack;
   int results, level = 1, z = 0;
   int i = 100;
   ackResultScan(&ebay, ack, results);
   while(ebay[++i]!='i' && 
      ebay.substr(i,string("item\"").length())!="item\"");
   i += string("item\"").length() + 1;
   stringstream html(ebay.substr(i,ebay.length()-i));
   getline(html, ebay, ',');
   while(level>0) {
      z += ebay.length();
      i = ebay.find("{");
      while(i!=-1){
         ebay.erase(ebay.begin()+i);
         level++;
         i = ebay.find("{");
      }
      i = ebay.find("}");
      while(i!=-1){
         ebay.erase(ebay.begin()+i);
         level--;
         i = ebay.find("}");
      }
      if(ebay.substr(1,5)=="title") {
         ebay = ebay.substr(10,ebay.length());
         ebay = ebay.substr(0,ebay.length()-2);
         temp.title = ebay;
      } else if (ebay.substr(1,8)=="viewItem") {
         ebay = ebay.substr(16,ebay.length());
         ebay = ebay.substr(0,ebay.length()-2);
         i = ebay.find("\\");
         while(i!=-1){
            ebay.erase(ebay.begin()+i);
            i = ebay.find("\\");
         }
         temp.viewItemURL = ebay;
      } else if (ebay.substr(1,13)=="sellingStatus") {
         getline(html, ebay, ',');
         i = ebay.find("{");
         while(i!=-1){
            ebay.erase(ebay.begin()+i);
            level++;
            i = ebay.find("{");
         }
         i = ebay.find("}");
         while(i!=-1){
            ebay.erase(ebay.begin()+i);
            level--;
            i = ebay.find("}");
         }
         if (ebay.substr(1,9)=="__value__") {
            trash = ebay.substr(13,9);
            trash = trash.substr(0,trash.length()-2);
            temp.price = stof(trash);
         }
      } else if (ebay.substr(1,11)=="listingInfo") {
         getline(html, ebay, ',');
         i = ebay.find("{");
         while(i!=-1){
            ebay.erase(ebay.begin()+i);
            level++;
            i = ebay.find("{");
         }
         i = ebay.find("}");
         while(i!=-1){
            ebay.erase(ebay.begin()+i);
            level--;
            i = ebay.find("}");
         }
         while(ebay.substr(1,7)!="endTime")
            getline(html, ebay, ',');
         temp.endTime = parseEbayTime(ebay.substr(13,19));
         base.push_back(temp);
      }
      getline(html, ebay, ',');
   }
   cout<<base.size()<<endl;
   return base;
}

time_t parseEbayTime(string ebay) {
   tm temp;
   temp.tm_year = atoi(ebay.substr(0,4).c_str()) - 1900;
   temp.tm_mon = atoi(ebay.substr(5,2).c_str()) - 1;
   temp.tm_mday = atoi(ebay.substr(8,2).c_str()) - 1;
   temp.tm_hour = atoi(ebay.substr(11,2).c_str()) - 1;
   temp.tm_min = atoi(ebay.substr(14,2).c_str()) - 1;
   temp.tm_sec = atoi(ebay.substr(17,2).c_str()) - 1;
   temp.tm_isdst = 0;
   return mktime(&temp);
}