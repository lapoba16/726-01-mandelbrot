window.onload = setTimeout(prepare, 500);

function log(msg) {
   document.getElementById('bbarl_log').innerHTML += msg;
}
function logln(msg) {
   document.getElementById('bbarl_log').innerHTML += 
      "<br>" + msg;
}
function stillActive() {
   if(document.getElementById('bbarl_active').innerHTML=="Loading.")
      document.getElementById('bbarl_active').innerHTML = "Loading..";
   else if(document.getElementById('bbarl_active').innerHTML=="Loading..")
      document.getElementById('bbarl_active').innerHTML = "Loading...";
   else if(document.getElementById('bbarl_active').innerHTML=="Loading...")
      document.getElementById('bbarl_active').innerHTML = "Loading"
   else
      document.getElementById('bbarl_active').innerHTML = "Loading.";
}

var allCars = [];
var innerDoc = "";
function prepare() {
   //getOld();
   var url = window.location + "";
   innerDoc = document.getElementsByTagName("iframe")[0].contentDocument || 
      document.getElementsByTagName("iframe")[0].contentWindow.document;
   if( !(url.search(/Auctions/i)+1) ){      //if search fails, returns -1 + 1 = 0 for if statement
      console.log("I DONT WANNA");
      return;
   }
   else
      console.log("OK FINE");
   //document.getElementById("PrintBlock").innerHTML = "";
   document.getElementsByTagName("footer")[0].
      getElementsByTagName("div")[0].innerHTML = 
   "<span id='bbarl_log_user' style='display:none;'></span>"

   +"<span class='bbarl_checks'>"
      +"<span>"
         +"<input id='bbarl_checkcontinue' type='checkbox'>"
         +"<label for='bbarl_checkcontinue'>Stop Logging</label>"
      +"</span><span>"
         +"<input id='bbarl_forcecheck' type='checkbox'>"
         +"<label for='bbarl_forcecheck'>Dump Current Car</label>"
      +"</span><span>"
         +"<input id='bbarl_expandAll' type='checkbox'>"
         +"<label for='bbarl_expandAll'>Expand All</label>"
      +"</span><span>"
         +"<input id='bbarl_collapseAll' type='checkbox'>"
         +"<label for='bbarl_collapseAll'>Collapse All</label>"
      +"</span><span>"
         +"<input id='bbarl_watchedOnly' type='checkbox'>"
         +"<label for='bbarl_watchedOnly'>Only Log Watched</label>"
      +"</span>"
   +"</span>"

   +"<span id='bbarl_active'></span><br>"
   +"<div id='bbarl_log'></div>"
   +"<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>";
   stillActive();
   newItem();
}
/*
   function getOld()
   {
      var i=0;
      allCars = [];
      while(localStorage.getItem(i)){
         allCars.push(localStorage.getItem(i));
         i++;
      }
   }
   function saveChanges()
   {
      localStorage.clear();
      for(var i=0; i<allCars.length; i++)
         localStorage.setItem(i, allCars[i]);
   }
*/
/******************************************************/
var currentBid = "";
var currentCar = "";
var currentDescription = "";
var currentImages = [];
var wasWatching = 0;
var onlyWatch = 0;

var recentBid = "";
var recentCar = "";
var debugging = 0;

function oldItem() {
   if(document.getElementById('bbarl_checkcontinue').checked) {
      document.getElementById("bbarl_active").innerHTML = "Stopped";
      return;
   }
   if(document.getElementById('bbarl_forcecheck').checked) {
      saveCar();
      document.getElementById('bbarl_forcecheck').checked = false;
   }
   onlyWatch = document.getElementById('bbarl_watchedOnly').checked;
   if(document.getElementById('bbarl_expandAll').checked) {
      for(var i=0; i<document.getElementById("bbarl_log").getElementsByClassName("bbarl_car_logged").length; i++) {
         document.getElementById("bbarl_log").
            getElementsByClassName("bbarl_car_logged")[i].
            getElementsByTagName("input")[0].checked = true;
      }
      document.getElementById('bbarl_expandAll').checked = false;
   }
   else if(document.getElementById('bbarl_collapseAll').checked) {
      for(var i=0; i<document.getElementById("bbarl_log").getElementsByClassName("bbarl_car_logged").length; i++) {
         document.getElementById("bbarl_log").
         getElementsByClassName("bbarl_car_logged")[i].
         getElementsByTagName("input")[0].checked = false;
      }
      document.getElementById('bbarl_collapseAll').checked = false;
   }
   stillActive();
   if(debugging==1) console.log("__Called oldItem()");
   var a = innerDoc.getElementsByClassName("auction-detail-lot-bidding-current-lot")[0];
   a = a.getElementsByTagName("table")[0];
   a = a.getElementsByTagName("tbody")[0];
   a = a.getElementsByTagName("tr")[0];
   a = a.getElementsByTagName("td")[0];
   a = a.getElementsByTagName("div")[1];
   a = a.getElementsByClassName("bidTimerContainer")[0];
   a = a.getElementsByTagName("svg")[0];
   a = a.getElementsByTagName("text")[0];
   recentBid = a.getElementsByTagName("tspan")[0].innerHTML;
   recentCar = (
      innerDoc.getElementsByClassName("auction-detail-lot-summary")[0].
      getElementsByTagName("table")[0].
      getElementsByTagName("tbody")[0].
      getElementsByTagName("tr")[0].
      getElementsByTagName("td")[0].
      getElementsByTagName("div")[0].innerHTML
   );
   if( recentCar != currentCar ){
      saveCar();
      //saveChanges();
      newItem();
   } else {
      currentBid = recentBid;
      window.setTimeout(oldItem, 1500);
   }
}

function newItem() {
   if(debugging==1) console.log("__Called newItem()");

   var a = innerDoc.getElementsByClassName("auction-detail-lot-bidding-current-lot")[0];
   a = a.getElementsByTagName("table")[0];
   a = a.getElementsByTagName("tbody")[0];
   a = a.getElementsByTagName("tr")[0];
   a = a.getElementsByTagName("td")[0];
   a = a.getElementsByTagName("div")[1];
   a = a.getElementsByClassName("bidTimerContainer")[0];
   a = a.getElementsByTagName("svg")[0];
   a = a.getElementsByTagName("text")[0];
   currentBid = a.getElementsByTagName("tspan")[0].innerHTML;
   if(debugging==1) console.log(currentBid);
   /*currentBid = (document.getElementsByClassName("arStartBid")[0].
      getElementsByTagName("strong")[1].innerHTML
   );*/
   /*var currentImagesDiv = 
      innerDoc.getElementsByClassName("auction-detail-lot-images")[0];
   currentImagesDiv = currentImagesDiv.getElementsByTagName("div")[0];
   currentImagesDiv = currentImagesDiv.getElementsByTagName("div")[1];
   currentImagesDiv = currentImagesDiv.getElementsByTagName("div")[0]; //  macro-carousels
   currentImagesDiv = currentImagesDiv.getElementsByTagName("div")[0]; //  image-carousel-main
   currentImagesDiv = currentImagesDiv.getElementsByTagName("div")[0]; //  image-carousel-thumb
   //currentImagesDiv = currentImagesDiv.getElementsByClassName("image-carousel-thumb-images")[0];
   if(currentImages.getElementsByTagName("div").length>0)
      currentImages = currentImages.getElementsByTagName("div")[0];
   currentImagesDiv = currentImagesDiv.getElementsByTagName("img");
   currentImages = [];
   for(var i=0; i<currentImagesDiv.length && i<5; i++){
      console.log(currentImages[i]);
      currentImages.push(currentImagesDiv[i].src);
   }*/
   currentCar = (
      innerDoc.getElementsByClassName("auction-detail-lot-summary")[0].
      getElementsByTagName("table")[0].
      getElementsByTagName("tbody")[0].
      getElementsByTagName("tr")[0].
      getElementsByTagName("td")[0].
      getElementsByTagName("div")[0].innerHTML
   );
   if(innerDoc.getElementsByClassName("scrollable ps-container ps-active-y").length>1)
      currentDescription = (
         innerDoc.getElementsByClassName("scrollable ps-container ps-active-y")[1].
         getElementsByTagName("table")[0].
         getElementsByTagName("tbody")[0]
      );
   else{
      currentDescription = (
         innerDoc.getElementsByClassName("scrollable ps-container ps-active-y")[0].
         getElementsByTagName("table")[0].
         getElementsByTagName("tbody")[0]
      );
   }
   wasWatching = 0;
   var waswatchingA = innerDoc.getElementsByClassName("scrollable ps-container ps-active-y")[0].
      getElementsByTagName("table")[0].
      getElementsByTagName("tbody")[0].
      getElementsByTagName("tr")[0].
      getElementsByTagName("td")[1];
   if(waswatchingA.getElementsByTagName("div").length>1){
      waswatchingA = waswatchingA.getElementsByTagName("div")[waswatchingA.getElementsByTagName("div").length-1];
      if(waswatchingA.getElementsByTagName("span").length>1){
         waswatchingA = waswatchingA.getElementsByTagName("span")[waswatchingA.getElementsByTagName("span").length-1];
         if(waswatchingA.className.search(/ng-hide/i)==-1)
            wasWatching = 1;
      }
   }

   var style_tag = "style='"
   +"' ";
   var mytablestyle = "";
   // ===============   IMAGES   =======================
   mytablestyle += "<div class='bbarl_car_logged_imgs'>";
   //a = innerDoc.getElementsByClassName("image-carousel-main-images")[0];
   a = innerDoc.getElementsByClassName("image-carousel-thumb-images")[0].getElementsByTagName("img");
   for(var i=0; i<a.length; i++)
      mytablestyle+="<span><img src='"+a[i].src+"'></span>";
   mytablestyle += "</div>";
   // ===============   /IMAGES  =======================
   // ===============   TABLE    =======================
   mytablestyle += "<table><tbody>";
   for(var i=0; i<currentDescription.getElementsByTagName("tr").length; i++) {
      mytablestyle += ("<tr><td>");

      if(currentDescription.getElementsByTagName("tr")[i].
      getElementsByTagName("td")[0].
      getElementsByTagName("div").length>0 ){
         if(currentDescription.getElementsByTagName("tr")[i].
         getElementsByTagName("td")[0].
         getElementsByTagName("div")[0].
         getElementsByTagName("span").length>0 )
            mytablestyle += (currentDescription.getElementsByTagName("tr")[i].
               getElementsByTagName("td")[0].
               getElementsByTagName("div")[0].
               getElementsByTagName("span")[0].innerHTML
            );
         else
            mytablestyle += (currentDescription.getElementsByTagName("tr")[i].
               getElementsByTagName("td")[0].
               getElementsByTagName("div")[0].innerHTML
            );
      }
      mytablestyle += ("</td><td>");

      if(currentDescription.getElementsByTagName("tr")[i].
      getElementsByTagName("td").length>1 &&
      currentDescription.getElementsByTagName("tr")[i].
      getElementsByTagName("td")[1].
      getElementsByTagName("div").length>0 ){
         if(currentDescription.getElementsByTagName("tr")[i].
         getElementsByTagName("td")[1].
         getElementsByTagName("div")[0].
         getElementsByTagName("div").length>0 ){
            for(var j=0; j< currentDescription.getElementsByTagName("tr")[i].
            getElementsByTagName("td")[1].
            getElementsByTagName("div")[0].
            getElementsByTagName("div").length; j++){
               if(currentDescription.getElementsByTagName("tr")[i].
               getElementsByTagName("td")[1].
               getElementsByTagName("div")[0].
               getElementsByTagName("div")[j].
               getElementsByTagName("span").length>0){
                  if(debugging==1 && j==0) log("A");
                  mytablestyle += currentDescription.getElementsByTagName("tr")[i].
                     getElementsByTagName("td")[1].
                     getElementsByTagName("div")[0].
                     getElementsByTagName("div")[j].
                     getElementsByTagName("span")[0].innerHTML+" ";
               }
               else{
                  if(debugging==1) log("B");
                  mytablestyle += currentDescription.getElementsByTagName("tr")[i].
                     getElementsByTagName("td")[1].
                     getElementsByTagName("div")[0].
                     getElementsByTagName("div")[j].innerHTML+" ";
               }
            }
         } else if (currentDescription.getElementsByTagName("tr")[i].
         getElementsByTagName("td")[1].
         getElementsByTagName("div")[0].
         getElementsByTagName("span").length > 0) {
            for(var j=0; j<currentDescription.getElementsByTagName("tr")[i].
            getElementsByTagName("td")[1].
            getElementsByTagName("div")[0].
            getElementsByTagName("span").length && j<1; j++){
               if(debugging==1 && j==0) log("C");
               mytablestyle += currentDescription.getElementsByTagName("tr")[i].
                  getElementsByTagName("td")[1].
                  getElementsByTagName("div")[0].
                  getElementsByTagName("span")[j].innerHTML+" ";
            }
         } else {
            if(debugging==1) log("D");
            mytablestyle += (currentDescription.getElementsByTagName("tr")[i].
               getElementsByTagName("td")[1].
               getElementsByTagName("div")[0].innerHTML
            );
         }
      }

      mytablestyle += ("</td></tr>");
   }
   mytablestyle += ("</tbody></table>");
   // ===============   /TABLE   =======================
   currentDescription = mytablestyle;
   window.setTimeout(oldItem, 1000);
}
var currentIdNum = 1;
function saveCar() {
   var currentChecks = [];
   for(var i=0; i<document.getElementById("bbarl_log").getElementsByClassName("bbarl_car_logged").length; i++) {
      currentChecks.push(document.getElementById("bbarl_log").getElementsByClassName("bbarl_car_logged")[i].getElementsByTagName("input")[0].checked);
   }
   currentIdNum += 1;
   var wholething = "<div class='bbarl_car_logged'>";
   wholething += "<input type='checkbox' id='bbarl_car_logged_"+currentIdNum+"'>";
   if(onlyWatch==0 || (onlyWatch==1 && wasWatching==1)){
      //currentBid = currentBid.replace(",","");
      wholething+= "<label for='bbarl_car_logged_"+currentIdNum+"'>";
      var watchlabel = "";
      if(wasWatching==1 || debugging==1){
         watchlabel = "<i class='fa fa-star ' " //fa-3x
         //+"box-shadow: 0px 0px 8px black;"
         +"style='color:cornflowerblue;'></i>";
      } else {
         watchlabel = "";
      }
      if(debugging==1) console.log("__Called saveCar()");
      wholething += "<div class='bbarl_carlabels'><span "
      +"class='bbarl_watched'><span>"+watchlabel+"</span></span>"
      +"<span class='bbarl_carName'><span>"+currentCar+"</span></span>"
      +"<span class='bbarl_carBid'><span>"+currentBid+"</span></span></div></label><br>";
      wholething += (currentDescription);
      wholething += "</div>";
      logln(wholething);
      for(var i=0; i<document.getElementById("bbarl_log").getElementsByClassName("bbarl_car_logged").length-1; i++) {
         document.getElementById("bbarl_log").getElementsByClassName("bbarl_car_logged")[i].getElementsByTagName("input")[0].checked = currentChecks[i];
      }
   }
}