function error(msg) {
   document.getElementById("errorLogging").innerHTML += msg + "<br>";
}
document.addEventListener('DOMContentLoaded', function() {
  onLoad();
});

function onLoad()
{
   chrome.tabs.executeScript(null, {file: "base.js"});
   return;
   error("I'm in!");
   chrome.tabs.executeScript( 
      null, 
      { code : "var long; long = document.getElementsByClassName('container').length; long"}, 
      function(result){
         error(result[0]);
      }
   );
}

/********************************************************************/

function catagoriesEventListeners(tagName)
{
   var catagories = document.getElementsByTagName(tagName);
   for(var i=0; i<catagories.length; i++)
      catagories[i].addEventListener('click',toggleCata,false);
   document.getElementById("wantsAllTitle").addEventListener('click',toggleAllWants,false);
   document.getElementById("newWants_submit").addEventListener('click',newItem,false);
   document.getElementById("saveItems_btn").addEventListener('click',saveItems,false);

   for(var i=0; i<WantsArray.length; i++)
      document.getElementsByName("item_button_"+i)[0].addEventListener('click',removeItem,false);

   for(var i=0; i<WantsArray.length; i++)
      document.getElementsByName("item_button_get_"+i)[0].addEventListener('click',getItem,false);


   document.getElementById('loadOriginal').addEventListener('click',loadOriginal,false);
}

function toggleCata()
{
   var catagory=this.innerHTML;
   if(this.style.color=="rgb(0, 0, 0)"){
      this.style.color="rgb(100, 100, 100)";
      document.getElementById("Wants_"+catagory).style.display = "none";
   }
   else{
      this.style.color="rgb(0, 0, 0)";
      document.getElementById("Wants_"+catagory).style.display = "initial";
   }
}

function toggleAllWants()
{
   if(this.style.color=="rgb(0, 0, 0)"){
      this.style.color="rgb(100, 100, 100)";
      document.getElementById("wantsAll").style.display = "none";
   }
   else{
      this.style.color="rgb(0, 0, 0)";
      document.getElementById("wantsAll").style.display = "initial";
   }
}

function quicksort(sortme, index)
{
   var less=[], greater=[], same=[];
   var j=0;
   var pivot, shorterWord;
   if(sortme.length>1){
   pivot=sortme[0][index];
      for(var i=0; i<sortme.length; i++){
         j = 0;
         shorterWord = ( sortme[i][index].length < pivot.length ) ? sortme[i][index].length : pivot.length ;
         while( j < shorterWord && sortme[i][index].charCodeAt(j)==pivot.charCodeAt(j))
            j++;
         if ( j == shorterWord )
            j--;
         if(sortme[i][index].charCodeAt(j)<pivot.charCodeAt(j))
            less.push(sortme[i]);
         else if (sortme[i][index].charCodeAt(j)>pivot.charCodeAt(j))
            greater.push(sortme[i]);
         else
            same.push(sortme[i]);
      }
      less=quicksort(less, index);
      greater=quicksort(greater, index);
      return less.concat(same, greater);
   }
   else
      return sortme;
}