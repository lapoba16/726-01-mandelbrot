var allColors = []
function startup()
{
   getOld();
   createChecks();
}
function getOld()
{
   var i=0;
   allColors = [];
   while(localStorage.getItem(i)){
      allColors.push(localStorage.getItem(i));
      i++;
   }
   document.getElementById("saving").innerHTML = allColors;
}
function saveChanges()
{
   localStorage.clear();
   for(var i=0; i<allColors.length; i++)
      localStorage.setItem(i, allColors[i]);
   document.getElementById("saving").innerHTML = allColors;
}
function createChecks()
{
   var cur = 0;
   var mysets = document.getElementsByClassName("gearset");
   for(var i=0; i<mysets.length; i++){
      var ULs = mysets[i].getElementsByTagName("ul");
      for(var j=0; j<ULs.length; j++){
         var LIs = ULs[j].getElementsByTagName("li");
         for(var k=0; k<LIs.length; k++){
            LIs[k].name = cur;
            //allColors[cur] = 1;
            LIs[k].addEventListener("click",toggleColor);
            if(allColors[cur]==1)
               LIs[k].style.backgroundColor = "white";
            else
               LIs[k].style.backgroundColor = "lightgray";
            cur++;
         }
      }
   }
}
function toggleColor()
{
   if(this.style.backgroundColor =="lightgray"){
      allColors[this.name] = 1;
      this.style.backgroundColor  = "white";
   }
   else{
      allColors[this.name] = 0;
      this.style.backgroundColor  = "lightgray";
   }
   saveChanges();
}
/*****************************************/
/*function populateWantsArray()
{
   var i=0;
   var workingWantsArray=[];
   while(localStorage.getItem(4*i)){
      workingWantsArray.push( [ localStorage.getItem(4*i), localStorage.getItem(4*i+1), localStorage.getItem(4*i+2), localStorage.getItem(4*i+3) ] );
      i++;
   }

   WantsArray=workingWantsArray;
}
function saveItems()
{
   WantsArray = quicksort(WantsArray, 3);

   localStorage.clear();
   for(var i=0; i<WantsArray.length; i++)
      for(var j=0; j<4; j++)
         localStorage.setItem(4*i+j, WantsArray[i][j]);   
}*/