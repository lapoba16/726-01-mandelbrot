#include <iostream>
#include <stdio.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_primitives.h>
/*#include <allegro5/allegro_color.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_memfile.h>
#include <allegro5/allegro_physfs.h>
#include <allegro5/allegro_native_dialog.h>*/

#ifndef MY_BOOLEAN_SET
#ifndef __cplusplus
typedef enum {false, true} bool;
#endif
#endif
#define nil NULL

#ifndef MyWindowClass
#define MyWindowClass
class Window {
public:
   ALLEGRO_DISPLAY* screen;
   ALLEGRO_EVENT_QUEUE* queue;
   ALLEGRO_EVENT_SOURCE 
      *user, 
      *mouse, 
      *keyboard,
      *window
   ;
   ALLEGRO_FONT *font12, *font18;
   int rate, width, height;
};
#endif

ALLEGRO_DISPLAY* allegro_spawn_window(int w, int h) {
   return al_create_display(w,h);
}
void allegro_init_mouse(Window* window) {
   ALLEGRO_EVENT_QUEUE *q = window->queue;
   window->mouse = al_get_mouse_event_source();
   al_register_event_source(q, window->mouse);
}
void allegro_init_keyboard(Window* window) {
   ALLEGRO_EVENT_QUEUE *q = window->queue;
   window->keyboard = al_get_keyboard_event_source();
   al_register_event_source(q, window->keyboard);
}
void allegro_init_user(Window* window) {
   ALLEGRO_EVENT_QUEUE *q = window->queue;
   ALLEGRO_EVENT_SOURCE *src = 
      (ALLEGRO_EVENT_SOURCE*)malloc(
         sizeof(ALLEGRO_EVENT_SOURCE)
      );
   al_init_user_event_source(src);
   al_register_event_source(q, src);
   window->user = src;
}
bool allegro_display_opt() {
   // #fcf
   al_set_new_display_option(ALLEGRO_RED_SIZE, 8, ALLEGRO_SUGGEST);
   al_set_new_display_option(ALLEGRO_GREEN_SIZE, 8, ALLEGRO_SUGGEST);
   al_set_new_display_option(ALLEGRO_BLUE_SIZE, 8, ALLEGRO_SUGGEST);
   //al_set_new_display_option(ALLEGRO_ALPHA_SIZE, 16, ALLEGRO_SUGGEST);
   //al_set_new_display_option(ALLEGRO_COLOR_SIZE, 64, ALLEGRO_SUGGEST);
   al_set_new_display_option(ALLEGRO_ALPHA_SIZE, 0, ALLEGRO_SUGGEST);
   al_set_new_display_option(ALLEGRO_COLOR_SIZE, 24, ALLEGRO_SUGGEST);
   // al_set_new_display_option(ALLEGRO_RED_SHIFT, 64, ALLEGRO_SUGGEST);
   // al_set_new_display_option(ALLEGRO_GREEN_SHIFT, 48, ALLEGRO_SUGGEST);
   // al_set_new_display_option(ALLEGRO_BLUE_SHIFT, 32, ALLEGRO_SUGGEST);
   // al_set_new_display_option(ALLEGRO_ALPHA_SHIFT, 16, ALLEGRO_SUGGEST);
   // 1 to indicate buffer pre-prepped, 2 for flipping/other method for
   //    next buffer in flip chain
   al_set_new_display_option(ALLEGRO_SWAP_METHOD, 0, ALLEGRO_SUGGEST);
   // 1 if regional update, 0 if al_display_region() == al_flip_display()
   al_set_new_display_option(ALLEGRO_UPDATE_DISPLAY_REGION, 0, ALLEGRO_SUGGEST);
   return true;
}
bool allegro_display_flags() {
   al_set_new_display_flags(ALLEGRO_WINDOWED);
   //al_set_new_display_flags(ALLEGRO_FULLSCREEN);
   //al_set_new_display_flags(ALLEGRO_FULLSCREEN_WINDOW);
   al_set_new_display_flags(ALLEGRO_RESIZABLE);
   //al_set_new_display_flags(ALLEGRO_OPENGL);
   //al_set_new_display_flags(ALLEGRO_OPENGL_3_0);
   //al_set_new_display_flags(ALLEGRO_OPENGL_FORWARD_COMPATIBLE);
   //al_set_new_display_flags(ALLEGRO_DIRECT3D);
   //al_set_new_display_flags(ALLEGRO_NOFRAME);
   return true;
}
bool allegro_update() {
   al_flip_display();
   return true;
}
bool allegro_clear_display() {
   al_clear_to_color(al_map_rgb_f(.85,.85,.85));
   allegro_update();
   return true;
}
Window* allegro_init(int w, int h) {
   Window* window = new Window();
   window->width = w;
   window->height = h;
   al_init();
   al_init_font_addon();
   al_init_ttf_addon();
   al_init_primitives_addon();
   /**/
   allegro_display_opt();
   allegro_display_flags();
   //al_set_new_window_position(0,0);     // set top left
   //al_set_new_refresh_rate(30);
   /*
   al_init_image_addon();
   al_init_font_addon();
   al_init_ttf_addon();
   */
   /**/
   al_install_mouse();
   al_install_keyboard();
   window->queue = al_create_event_queue();
   /**/
   allegro_init_user(window);
   allegro_init_mouse(window);
   allegro_init_keyboard(window);
   window->screen = allegro_spawn_window(w, h);
   al_register_event_source(
      window->queue, 
      al_get_display_event_source(window->screen)
   );
   window->rate = al_get_new_display_refresh_rate();
   window->font12 = al_load_ttf_font("./DejaVuSans.ttf",12,0);
   window->font18 = al_load_ttf_font("./DejaVuSans.ttf",18,0);
   /**/
   allegro_clear_display();
   return window;
}
void allegro_lock_mouse(bool yesno, ALLEGRO_DISPLAY* screen = NULL) {
   if (yesno && screen!=NULL) 
      al_grab_mouse(screen);
   else
      al_ungrab_mouse();
}
void allegro_hide_mouse(ALLEGRO_DISPLAY* screen) {
   al_hide_mouse_cursor(screen);
}
void allegro_show_mouse(ALLEGRO_DISPLAY* screen) {
   al_show_mouse_cursor(screen);
}
void allegro_destroy(Window *window) {
   al_destroy_user_event_source(window->user);
}

/*
#ifdef __cplusplus
extern "C" {
#endif
   // ...
#ifdef __cplusplus
}
#endif
*/
