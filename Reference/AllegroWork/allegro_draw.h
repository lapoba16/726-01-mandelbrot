#include <stdio.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_color.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>

using namespace std;

void ad_line(float x1, float y1, float x2, float y2,
   float thickness=0, ALLEGRO_COLOR color=al_map_rgb_f(0,0,0)) {
   al_draw_line(x1, y1, x2, y2, color, thickness);
   al_flip_display();
}
void ad_rect(float x1, float y1, float x2, float y2,
   ALLEGRO_COLOR color=al_map_rgb_f(0,0,0), 
   float thickness=0, bool filled=false) {
   if(!filled)
      al_draw_rectangle(x1, y1, x2, y2, color, thickness);
   else
      al_draw_filled_rectangle(x1, y1, x2, y2, color);
   al_flip_display();
}
void ad_round_rect(float x1, float y1, float x2, float y2,
   float rx, float ry, 
   ALLEGRO_COLOR color=al_map_rgb_f(0,0,0), float thickness=0,
   bool filled=false) {
   if(!filled)
      al_draw_rounded_rectangle(x1, y1, x2, y2,
         rx, ry, color, thickness);
   else
      al_draw_filled_rounded_rectangle(x1, y1, x2, y2,
         rx, ry, color);
   al_flip_display();
}
void ad_circle(float cx, float cy, float r, 
   ALLEGRO_COLOR color=al_map_rgb_f(0,0,0),
   float thickness=0, bool filled=false){
   if(!filled)
      al_draw_circle(cx, cy, r, color, thickness);
   else
      al_draw_filled_circle(cx, cy, r, color);
   al_flip_display();
}
void ad_ellipse(float cx, float cy, float rx, float ry,
   ALLEGRO_COLOR color=al_map_rgb_f(0,0,0), 
   float thickness=0, bool filled=false) {
   if(!filled)
      al_draw_ellipse(cx, cy, rx, ry, color, thickness);
   else
      al_draw_filled_ellipse(cx, cy, rx, ry, color);
   al_flip_display();
}
