#include <allegro5/allegro.h>
#include <allegro5/allegro_color.h>

#define LIGHTBLUE al_map_rgb_f(.5,.5,1)
#define RED al_map_rgb_f(1,0,0)
#define GREEN al_map_rgb_f(0,1,0)
#define BLUE al_map_rgb_f(0,0,1)
#define WHITE al_map_rgb_f(1,1,1)
#define BLACK al_map_rgb_f(0,0,0)
#define GRAY al_map_rgb_f(.5,.5,.5)
#define LIGHTGRAY al_map_rgb_f(.75,.75,.75)
#define DARKGRAY al_map_rgb_f(.25,.25,.25)