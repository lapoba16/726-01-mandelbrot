#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <stdio.h>

bool aeq_empty(ALLEGRO_EVENT_QUEUE* events) {
   return al_is_event_queue_empty(events);
}
ALLEGRO_EVENT* aeq_pop(ALLEGRO_EVENT_QUEUE* events) {
   ALLEGRO_EVENT *i;// = (ALLEGRO_EVENT*)malloc(sizeof(ALLEGRO_EVENT));
   i = NULL;
   if (al_get_next_event(events, i))
      return i;
   return NULL;
}
void aeq_peek(ALLEGRO_EVENT_QUEUE* events, ALLEGRO_EVENT **happen) {
   if(*happen==NULL)
      *happen = (ALLEGRO_EVENT*)malloc(sizeof(ALLEGRO_EVENT));
   al_peek_next_event(events, *happen);
}
void aeq_skip(ALLEGRO_EVENT_QUEUE* events) {
   al_drop_next_event(events);
}
void aeq_clear(ALLEGRO_EVENT_QUEUE* events) {
   al_flush_event_queue(events);
}
void aeq_wait_pop(ALLEGRO_EVENT_QUEUE *events, ALLEGRO_EVENT **happen) {
   if(*happen==NULL)
      *happen = (ALLEGRO_EVENT*)malloc(sizeof(ALLEGRO_EVENT));
   al_wait_for_event(events, *happen);
}
void aeq_wait_peek(ALLEGRO_EVENT_QUEUE *events, ALLEGRO_EVENT **happen) {
   if(*happen==NULL)
      *happen = (ALLEGRO_EVENT*)malloc(sizeof(ALLEGRO_EVENT));
   ALLEGRO_EVENT* fail = NULL;
   al_wait_for_event(events, fail);
   aeq_peek(events, happen);
}
void aeq_wait(ALLEGRO_EVENT_QUEUE *events, ALLEGRO_EVENT **happen) {
   aeq_wait_pop(events, happen);
}